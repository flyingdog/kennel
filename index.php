<?php
	error_reporting(E_ALL ^ E_DEPRECATED);
	require_once('system/libraries/Kennel.php');

/*
                                                 ,:
                                               ,' |
                                              /   :
                                           --'   /
                                           \/ />/
                                           / <//_\
                                        __/   /
                                        )'-. /
                                        ./  :\
                                         /.' '
                                       '/'
                                       +
                                      '
                                    `.
                                .-"-
                               (    |
                            . .-'  '.
                           ( (.   )8:
                       .'    / (_  )
                        _. :(.   )8P  `
                    .  (  `-' (  `.   .
                     .  :  (   .a8a)
                    /_`( "a `a. )"'
                (  (/  .  ' )=='
               (   (    )  .8"   +
                 (`'8a.( _(   (
              ..-. `8P    ) `  )  +
            -'   (      -ab:  )
          '    _  `    (8P"Ya
        _(    (    )b  -`.  ) +
       ( 8)  ( _.aP" _a   \( \   *
     +  )/    (8P   (88    )  )
        (a:f   "     `"       `
*/

	Kennel::init();

  if ( array_key_exists('HTTP_HOST', $_SERVER) ) request::process();
?>