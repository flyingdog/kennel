for toggle in document.getElementsByTagName('button') when toggle.className.split(' ').indexOf('kennel-toggle-collapse') >= 0
	do (toggle) ->
		pres = toggle.parentNode.parentNode.getElementsByTagName 'pre'
		pres = (pre for pre in pres when pre.className.split(' ').indexOf('kennel-collapsible') >=0)
		toggle.onclick = (event) ->
			event.preventDefault()
			for pre in pres
				classes = pre.className.split ' '
				index = classes.indexOf 'active'
				if index < 0 then classes.push 'active' else classes.splice index, 1
				pre.className = classes.join ' '