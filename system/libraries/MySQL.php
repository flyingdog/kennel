<?php
	
	class MySQL {
		public static $host;
		public static $user;
		public static $pass;
		public static $database;
		private static $CONN;
		
		public static $last_query;
		public static $num_queries = 0;
		private static $structure_cache = array();
		
		//constructor/destructor functions
		//////////////////////////////////
		function __construct()
		{
			if ( !self::$CONN ) self::connect();
		}
		
		public static function connect($host=null, $user=null, $pass=null, $database=null)
		{
			if ( self::$CONN && @mysql_ping(self::$CONN) ) return;
			
			self::$host = Kennel::getSetting('database','host');
			self::$user = Kennel::getSetting('database','user');
			self::$pass = Kennel::getSetting('database','pass');
			self::$database = Kennel::getSetting('database','database');
			
			self::$CONN = mysql_connect(($host?$host:self::$host), ($user?$user:self::$user), ($pass?$pass:self::$pass));
			if ( function_exists('mysql_set_charset') )
				mysql_set_charset('utf-8', self::$CONN);
			mysql_select_db(($database?$database:self::$database), self::$CONN);
		}
		
		function __destruct()
		{
			@mysql_close(self::$CONN);
		}
		
		//query functions
		/////////////////
		public function query($sql)
		{
			self::connect();
			
			self::$num_queries++;
			$rs = mysql_query($sql, self::$CONN);
			self::$last_query = $sql;
			if ( mysql_error() ) self::dumpError($sql);
			else return $rs;
		}
		
		public function dumpError($sql)
		{
		  if ( Kennel::$ROOT_URL && !request::isAjax() )
		  {
		    // Non-ajax HTTP Request
  			$table = XML::element('table', null, array('class'=>'kennel-debug'));
			
  			$tr = XML::element('tr', $table);
  			$th = XML::element('th', $tr, array('colspan'=>'2'), "SQL query returned an error");
			
  			$tr = XML::element('tr', $table);
  			$th = XML::element('th', $tr, null, 'query');
  			$td = XML::element('td', $tr, null, syntax::mysql($sql));
			
  			$tr = XML::element('tr', $table);
  			$th = XML::element('th', $tr, null, 'error');
  			$td = XML::element('td', $tr, null, mysql_error(self::$CONN));
			
  			$full_backtrace = debug_backtrace();
  			$backtrace = $full_backtrace[3];
			
  			if ( isset($backtrace['file']) )
  			{
  				$tr = XML::element('tr', $table);
  				$th = XML::element('th', $tr, null, 'file');
  				$td = XML::element('td', $tr, null, $backtrace['file']);
  			}
			
  			if ( isset($backtrace['line']))
  			{
				
  				$tr = XML::element('tr', $table);
  				$th = XML::element('th', $tr, null, 'line');
  				$td = XML::element('td', $tr, null, $backtrace['line']);
  			}
			
  			if ( isset($backtrace['class']) )
  			{
  				$tr = XML::element('tr', $table);
  				$th = XML::element('th', $tr, null, 'class');
  				$td = XML::element('td', $tr, null, $backtrace['class']);
  			}
			
  			if ( isset($backtrace['function']) )
  			{
  				$tr = XML::element('tr', $table);
  				$th = XML::element('th', $tr, null, 'function');
  				$td = XML::element('td', $tr, null, $backtrace['function']);
  			}

  			$style = html::css('kennel-debug.css');

  			echo $style.$table;
  			debug::backtrace(2);
		  }
		  else
		  {
		    // Ajax or cli
		    $default = "\033[0m";
		    
		    echo "\n\033[1;30;41m"; // red background, bold
		    echo "SQL query returned an error:{$default}\n";
		    
		    $lines = explode("\n", $sql);
		    foreach ( $lines as $line )
		    {
  		    echo "  {$line}\n";
		    }
		    
		    $error = mysql_error(self::$CONN);
		    echo "\033[31m"; // red
		    echo "{$error}{$default}\n\n";
		  }
			
			die();
		}
		
		public function queryFetch($sql)
		{
			$rs = self::query($sql);
			return self::fetch($rs);
		}
		
		//result functions
		//////////////////
		public function numRows($rs)
		{
			return mysql_num_rows($rs);
		}
		
		public function affected()
		{
			return mysql_affected_rows();
		}
		
		public function insertId()
		{
			return mysql_insert_id(self::$CONN);
		}
		
		//fetch functions
		/////////////////
		public function fetch($rs)
		{
			if ( gettype($rs) != "resource" ) return false;
			else return mysql_fetch_object($rs);
		}
		
		public function fetchObject($rs=null)
		{
			if ( gettype($rs) != "resource" ) return false;
			else return mysql_fetch_object($rs);
		}
		
		public function fetchRow($rs=null)
		{
			if ( gettype($rs) != "resource" ) return false;
			else return mysql_fetch_row($rs);
		}
		
		public function fetchArray($rs=null)
		{
			if ( gettype($rs) != "resource" ) return false;
			else return mysql_fetch_array($rs);
		}
		
		public function fetchAssoc($rs=null)
		{
			if ( gettype($rs) != "resource" ) return false;
			else return mysql_fetch_assoc($rs);
		}
		
		public function fetchAll($rs)
		{
			if ( gettype($rs) != "resource" ) return false;
			$ret = array();
			while ( $obj = mysql_fetch_object($rs) )
				$ret[] = $obj;
			return $ret;
		}
		
		public function fetchAllArray($rs) {
			if ( gettype($rs) != "resource" ) return false;
			$ret = array();
			while ( $obj = mysql_fetch_array($rs) )
				$ret[] = $obj;
			return $ret;
		}
		
		//misc functions
		////////////////
		static function escape($input)
		{
			self::connect();
			
			if ( is_string($input) )
			{
				return mysql_real_escape_string($input, self::$CONN);
			}
			elseif (is_array($input))
			{
				$arr = array();
				foreach ($input as $key=>$element)
					$arr[$key] = self::escape($element, self::$CONN);
				return $arr;
			}
			else
			{
				return $input;
			}
		}
		
		public function dateToUs($str)
		{
			if ( !$str ) return false;
			$split = split("/",$str);
			if ( !$split[2] || !$split[1] || !$split[0] ) return false;
			return $split[2]."-".$split[1]."-".$split[0];
		}
		
		public function dateToBr($str)
		{
			if ( !$str ) return false;
			$split = split("-",$str);
			if ( !$split[2] || !$split[1] || !$split[0] ) return false;
			return $split[2]."/".$split[1]."/".$split[0];
		}
		
		public function numQueries()
		{
			return self::$num_queries;
		}
		
		public function getTableStructure($table)
		{
			if ( array_key_exists($table, self::$structure_cache) )
				return self::$structure_cache[$table];
			
			$structure = array();
			$rs = self::query("DESC {$table}");
			while ( $row = self::fetch($rs) )
			{
				unset($field);

				$field = array(
					'name'=>$row->Field,
					'type'=>$row->Type,
					'requiresd'=>$row->Null=="NO"?0:1,
					'default_value'=>$row->Default,
					'value'=>$row->Default?$row->Default:null
				);
				
				$structure[$row->Field] = $field;
			}
			
			self::$structure_cache[$table] = $structure;
			return $structure;
		}
		
	}
?>
