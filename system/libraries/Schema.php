<?php
	class Schema
	{
		var $model_name;
		var $table;

		var $fields = array();
		var $relationships = array();
		var $uniqueKeys = array();
		
		/**
		 * The constructor method receives $model_name and load it's respective XML
		 * description into the class. Each field also instantiates a Field object,
		 * and each relationship instantiates both a Field and a Relationship
		 * object.
		 *
		 * @return void
		 **/
		function __construct($model_name)
		{
			$this->model_name = $model_name;

			$path = cascade::schema($model_name);
			if ( !$path )
				debug::error("Schema::__construct - model schema for \"{$model_name}\" not found.", null, 1);

			$doc = new DOMDocument;
			$doc->load(realpath($path));
			
			$root = $doc->getElementsByTagName('model')->item(0);
			if ( Kennel::getSetting('database', 'prefix') )
				$this->table = Kennel::getSetting('database', 'prefix') . '_' . $root->getAttribute('table');
			else
				$this->table = $root->getAttribute('table');
			
			// Inject the id field
			$id = new Field($this->table, null);
			$id->name = 'id';
			$id->type = 'int';
			$id->size = 12;
			$id->primaryKey = true;
			$this->fields['id'] = $id;

			// Load the regular fields
			$fields = $doc->getElementsByTagName('field');
			foreach ( $fields as $element )
			{
				$field = new Field($this->table, $element);
				$this->fields[$field->name] = $field;
			}

			// Load the relationships
			$relationships = $doc->getElementsByTagName('relationship');
			foreach ( $relationships as $element )
			{
				// Register the relationship
				$relationship = new Relationship($model_name, $element);
				$this->relationships[$relationship->name] = $relationship;

				// Register the foreign key (and type for polymorphic associations)
				$fields = $relationship->foreignKeyAndTypeFields($this->table);
				foreach ( $fields as $field )
				{
					$this->fields[$field->name] = $field;
				}
			}

			// Load the unique keys
			$uniqueKeys = $doc->getElementsByTagName('unique-key');
			foreach ( $uniqueKeys as $element )
			{
				$uniqueKey = new UniqueKey($element);
				$this->uniqueKeys[$uniqueKey->name] = $uniqueKey;
			}
		}
		
		function __get($name)
		{
			if ( array_key_exists($name, $this->fields) )
				return $this->fields[$name];
			else if ( array_key_exists($name, $this->relationships) )
				return $this->relationships[$name];
			else
				return null;
		}

		function relationships_to($destination)
		{
			$rels;
			foreach ( $this->relationships as $name => $rel )
			{
				// if ( $rel->as === $destination || $rel->name === )
			}
			return $rels;
		}
		
		function primary_key()
		{
			foreach ( $this->fields as $field )
				if ( isset($field->primaryKey) )
					return $field;
		}
		
		public function createString()
		{
			$sql = "CREATE TABLE IF NOT EXISTS `{$this->table}`";
			
			$fields = array();
			foreach($this->fields as $field)
			{
				$fields[] = $field->createString();
			}

			foreach ( $this->uniqueKeys as $uniqueKey )
			{
				$fields[] = $uniqueKey->createString();
			}
			
			$sql .= " (\n" . implode(", \n", $fields) . "\n)";

			$sql .= ' CHARSET=utf8;';
			
			return $sql;
		}

		public function dump($return=false)
		{
			$table = XML::element('table', null, array('class'=>'kennel-debug'));

			$tr = XML::element('tr', $table);
			$th = XML::element('th', $tr, array('colspan'=>2), "{$this->model_name} ");
			XML::element('code', $th, null, $this->table);

			$tr = XML::element('tr', $table);
			$th = XML::element('th', $tr, array(
				'colspan'=>2, 'style'=>'text-align:center'
				), 'fields');

			foreach ( $this->fields as $field )
			{
				$tr = XML::element('tr', $table);
				XML::element('th', $tr, null, $field->name);
				XML::element('td', $tr, null, $field->type
					. ($field->size ? " ({$field->size})" : ''));
			}

			$tr = XML::element('tr', $table);
			$th = XML::element('th', $tr, array(
				'colspan'=>2, 'style'=>'text-align:center'
				), 'relationships');

			foreach ( $this->relationships as $rel )
			{
				$tr = XML::element('tr', $table);
				XML::element('th', $tr, null, $rel->name);
				XML::element('td', $tr, null, $rel->type);
				XML::element('td', $tr, null, $rel->destination);
				if ( $rel->as )
					XML::element('td', $tr, null, $rel->as);
			}

			$style = html::css('kennel-debug.css');
			if ( $return ) return $style . $table;
			else echo $style . $table;
		}
		
	}
?>
