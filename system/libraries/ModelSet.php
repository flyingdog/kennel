<?php
	/**
	 * ModelSet is an iterator that contains a set of models from a one-to-many
	 * relationship. It serves as a proxy between the parent and the children.
	 **/
	class ModelSet implements Iterator
	{
		private $_parent = null;
		private $_relationship = null;
		private $_models = array();
		private $_loaded = false;

		var $model_name;

		/**
		 * The constructor takes the parent $model and the relevant $relationship,
		 * then queries the database to generate the set of foreign model instances.
		 *
		 * @param Model $parent
		 * @param Relationship $relationship
		 * @return void
		 **/
		public function __construct(Model $parent, Relationship $relationship)
		{
			$this->_parent = $parent;
			$this->_relationship = $relationship;
			$this->model_name = $relationship->destination;
		}

		/**
		 * When retrieving a model instance, to-many relationships are assigned an
		 * empty ModelSet, which lazily load it's managed models when required.
		 *
		 * @return void
		 **/
		private function load()
		{
			// Lazy loading
			$this->_loaded = true;

			if ( !$this->_parent->id ) return;

			$this->_models = ORM::retrieve($this->criteria());

			// Inverse association
			foreach ( $this->_models as $model )
				$this->inverseAssociate($model);
		}

		/**
		 * Returns a Criteria object that can be used to retrieve the models
		 * in this set.
		 *
		 * @return Criteria
		 **/
		private function criteria()
		{
			$c = new Criteria($this->_relationship->destination);

			if ( $this->_relationship->polymorphic )
			{
				$c->add("{$this->_relationship->as}_id", $this->_parent->id);
				$c->add("{$this->_relationship->as}_model", $this->_relationship->origin);
				$c->add("{$this->_relationship->as}_rel", $this->_relationship->name);
			}
			else
			{
				$c->add("{$this->_relationship->origin}_id", $this->_parent->id);
			}

			return $c;
		}

		/**
		 * Associates $model with this set's parent
		 *
		 * @return void
		 **/
		private function inverseAssociate($model)
		{
				if ( $this->_relationship->polymorphic )
				{
					$model->assoc($this->_relationship->as, $this->_parent, $this->_relationship->name);
				}
				else
				{
					foreach ( $model->schema->relationships as $inverse)
						if ( $inverse->type === Relationship::BELONGS_TO
							&& $inverse->destination == $this->_parent->model_name )
							$model->{$inverse->name} = $this->_parent;
				}
		}

		/**
		 * undocumented function
		 *
		 * @return void
		 * @author 
		 **/
		public function isValid()
		{
			return true;
		}

		/**
		 * Associates a model to the has_many relationship set.
		 *
		 * @param Model $model
		 * @return Model
		 **/
		public function add(Model $model, $lazyload=true)
		{
			if ( $lazyload && !$this->_loaded ) $this->load();

			// Validate the association
			if ( $this->_relationship->destination !== $model->model_name )
				debug::error("ModelSet error: trying to add an instance of
					<code>{$model->model_name}</code> to the set
					<code>{$this->_parent->model_name}.{$this->_relationship->name}</code>
					({$this->_relationship->destination})");

			// Model is already associated
			if ( in_array($model, $this->_models) )
				return $model;

			// Model is already associated, with another instance
			foreach ( $this->_models as $existing )
				if ( $existing->id == $model->id )
					return $model;

			// Local association
			$this->_models[] = $model;

			// Inverse Association
			$this->inverseAssociate($model);

			// Foreign key assignment
			// $this->_parent->assign_foreign_key($this->_relationship, $model);
		}

		/**
		 * Deletes the whole set from the database.
		 *
		 * @return void
		 **/
		public function delete()
		{
			if ( !$this->_loaded ) $this->load();

			foreach ( $this->_models as $key=>$model )
			{
				$model->delete();
				unset($this->_models[$key]);
			}
		}

		/**
		 * Deletes the whole set from the database.
		 *
		 * @return void
		 **/
		public function save()
		{
			foreach ( $this->_models as $model )
				$model->save();
		}

		/**
		 * Remove $model from the set and delete it's database record.
		 *
		 * @param Model $model
		 * @return void
		 **/
		public function remove(Model $model)
		{	
			$key = array_search($model, $this->_models);

			if ( $key === false )
				debug::error("ModelSet error: trying to remove a model that is not
					in this set");

			unset($this->_models[$key]);
			$model->delete();
		}

		/**
		 * Magic method that allows assigning a $value to a $proprety in every
		 * model in the set.
		 *
		 * @param string $name
		 * @param string $value
		 * @return void
		 **/
		public function __set($property, $value)
		{
			if ( !$this->_loaded ) $this->load();

			foreach ( $this->_models as $model )
				$model->$property = $value;
		}

		/**
		 * Returns all the models in the set ordered by $field_name
		 *
		 * @param $field_name
		 * @return array
		 * @return array
		 **/
		public function sortBy($field_name, $descending=false)
		{
			if ( !$this->_loaded ) $this->load();

			$array = array();
			foreach ( $this->_models as $model )
				$array[$model->order] = $model;

			ksort($array, SORT_NUMERIC);
			if ( $descending ) $array = array_reverse($aray);

			return $array;
		}

		/**
		 * Returns the number of models in this set
		 *
		 * @return int
		 **/
		public function count()
		{
			if ( $this->_loaded )
			{
				return count($this->_models);
			}
			else
			{
				return ORM::count($this->criteria());
			}


			return count($this->_models);
		}

		/**
		 * Prints out the contents of the set.
		 *
		 * @param boolean $return
		 * @param boolean $recursive
		 * @return void or string
		 **/
		public function dump($recursive=true, $return=false, &$recursion_stack=array())
		{
			if ( !$this->_loaded ) $this->load();

			$table = XML::element('table', null, array('class'=>'kennel-debug'));

			$backtrace = debug_backtrace(1);
  		if ( !isset($backtrace[1]['class']) || ( isset($backtrace[1]['class']) && !in_array($backtrace[1]['class'], array('Model', 'ModelSet')) ) )
  		{
				$tr = XML::element('tr', $table);
				$td = XML::element('td', $tr, array('colspan'=>2));
				$td = XML::element('small', $td, null, $backtrace[0]['file'] . ' : ' . $backtrace[0]['line']);
  		}

			$tr = XML::element('tr', $table);
			$td = XML::element('td', $tr, array('class'=>'set'));

			if ( !empty($this->_models) )
			{
				foreach ( $this->_models as $model )
				{
					$td->text = $model->dump($recursive, true, $recursion_stack);
				}
			}
			else
			{
				if ( $this->_parent->id )
					$td->text = XML::element('div', null, array('class'=>'empty'), 'empty');
				else
					$td->text = XML::element('div', null, array('class'=>'empty'), 'not loaded');
			}

			if ( $return ) return $table;
			else echo $table;
		}

		/**
		 * Returns the first model in the set, or null if the set is empty.
		 *
		 * @return Model
		 **/
		public function first()
		{
			if ( !$this->_loaded ) $this->load();

			if ( isset($this->_models[0]) )
				return $this->_models[0];
			else
				return null;
		}

		/**
		 * Returns the last model in the set, or null if the set is empty.
		 *
		 * @return Model
		 **/
		public function last()
		{
			if ( !$this->_loaded ) $this->load();

			if ( empty($this->_models) )
				return null;
			else
				return $this->_models[count($this->_models)-1];
		}

		/**
		 * Returns the model at $index, or null if the index is not set.
		 *
		 * @return Model
		 **/
		public function index($index)
		{
			if ( !$this->_loaded ) $this->load();

			if ( isset($this->_models[$index]) )
				return $this->_models[$index];
			else
				return null;
		}

		/**
		 * Returns the model with id = $id, or null if not found.
		 *
		 * @param int $id
		 * @return Model
		 **/
		public function id($id)
		{
			if ( !$this->_loaded ) $this->load();

			foreach ( $this->_models as $model )
				if ( $model->isValid() && $model->id  === (int) $id )
					return $model;
			
			return null;
		}

		/**
		 * Iterator::rewind implementation
		 *
		 * @return void
		 **/
		public function rewind()
		{
			if ( !$this->_loaded ) $this->load();

			reset($this->_models);
		}
		
		/**
		 * Iterator::current implementation
		 *
		 * @return Model
		 **/
		public function current()
		{
			if ( !$this->_loaded ) $this->load();

			return current($this->_models);
		}
		
		/**
		 * Iterator::key implementation
		 *
		 * @return int
		 **/
		public function key()
		{
			if ( !$this->_loaded ) $this->load();

			return key($this->_models);
		}
		
		/**
		 * Iterator::next implementation
		 *
		 * @return Model
		 **/
		public function next()
		{
			if ( !$this->_loaded ) $this->load();

			next($this->_models);
		}
		
		/**
		 * Iterator::valid implementation
		 *
		 * @return boolean
		 **/
		public function valid()
		{
			if ( !$this->_loaded ) $this->load();

			return array_key_exists(key($this->_models), $this->_models);
		}

	} // END class
?>