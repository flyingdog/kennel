<?php
	
	class Controller
	{
		/**
		 * An empty constructor is provided to avoid issues with legacy controllers
		 * calling ``parent::__construct()``
		 *
		 * @return void
		 **/
		public function __construct()
		{
		}
		
		/**
		 * Default called if the requested URI does not match any registered
		 * routes. Can be invoked as ``$this->notfound()`` from any
		 * controller that extends this class. The 404 view can be overriden simply
		 * by placing a ``/views/404.php`` file in the application or any
		 * module, but if you need controller functionality for this, you can
		 * register the "catch all" ``*`` route with any action you want.
		 * 
		 * @return void
		 **/
		public function get_notfound()
		{
			$view = new View("404");
			$view->render();
		}
		
		/**
		 * Alias to request::redirect($location, $status);
		 *
		 * @param string $location
		 * @param int $status
		 * @return void
		 **/
		protected function redirect($location, $status=303)
		{
		  request::redirect($location, $status);
		}

		/**
		 * Parses $className and return it's controller reference.
		 *
		 * @param string $className
		 * @return string
		 **/
		public static function reference($className)
		{
			// Cms_AlbumReview
			// cms/album_review

			$className = substr_replace($className, '', -11);
			$segments = explode('_', $className);
			foreach ($segments as $k=>$segment)
			{
				$word = strtolower($segment[0]);
				for ( $i=1; $i < strlen($segment); $i++ )
				{
					$char = $segment[$i];
					if ( ctype_upper($char)) $word .= '-' . strtolower($char);
					else $word .= $char;
				}
				$segments[$k] = $word;
			}
			return implode('/', $segments);
		}

		/**
		 * Parses the controller $reference and returns it's class name in PascalCase
		 * and underscores separating subfolders.
		 *
		 * @param string $reference
		 * @return string
		 **/
		public static function class_name($reference)
		{
			$segments = explode('/', $reference);
			$className = '';
			foreach ($segments as $segment)
			{
				$words = explode('-', $segment);
				$pascalCase = '';
				foreach ($words as $word)
				{
					$pascalCase .= ucfirst($word);
				}
				$className .= $pascalCase . '_';
			}
			
			return $className .= 'controller';
		}

		/**
		 * Receives the $action_method and returns a method name in camelCase
		 * prefixed by an HTTP method and an underscore.
		 *
		 * @param string $action_method
		 * @return string
		 */
		public static function method_name($action_method)
		{
			$words = explode('-', $action_method);
			$camelCase = '';
			foreach ($words as $n => $word)
			{
				if ($n === 0) $camelCase .= strtolower($word);
				else $camelCase .= ucfirst($word);
			}
			return $camelCase;
		}

		/**
		 * Parses the controller $reference and return the applicable route.
		 *
		 * @param string $reference
		 * @param string $action
		 * @return string
		 **/
		public static function actionRoute($reference, $action)
		{
			$segments = explode('/', $reference);
			foreach ( $segments as $i => $segment )
			{
				// If this is the last segment and is named 'main', the route segment
				// should be empty as this is a root controller.
				if ( $i + 1 == count($segments) && $segment == 'main' )
					unset($segments[$i]);
			}

			// If the action is the index, the route segment should be empty.
			if ( $action != 'index' ) $segments[] = str_replace('_', '-', $action);

			return '/' . implode('/', $segments);
		}

		/**
		 * Parse the $action string and return a valid array callback.
		 *
		 * @param string $http_verb
		 * @param string $action
		 * @return array
		 **/
		public static function callback($http_verb, $action)
		{
			$class_name = self::class_name(strtok($action, ':'));
			$class_method = strtolower($http_verb) . '_' . self::method_name(strtok(':'));

			return array($class_name, $class_method);
		}

		/**
		 * Create and return a Controller instance from the $controller reference
		 * string.
		 *
		 * @param 
		 * @return Controller $instance
		 **/
		public static function instance($controller)
		{
			$class_name = self::class_name($controller);
			return new $class_name();
		}

		/**
		 * Receives a valid $action string, instantializes the controller class
		 * and call the relevant method, passing any parameters present.
		 *
		 * Returns the return value of the method call.
		 *
		 * @param string $http_verb
		 * @param string $action
		 * @param array &$parameters
		 * @return mixed
		 **/
		public static function call($http_verb, $action, $parameters=array())
		{
			list($class_name, $class_method) = self::callback($http_verb, $action);
			$controller_instance = self::instance(strtok($action, ':'));
			return call_user_func_array(array($controller_instance, $class_method), $parameters);
		}

		/**
		 * Scans the project directory tree for controllers, and returns an array
		 * with the controller references as keys and paths as values. If two
		 * controllers with the same reference are found, the cascading priority
		 * determines which will be included in the returning array and which
		 * will be ignored.
		 *
		 * @return array
		 **/
		public static function paths()
		{
			// Retrieve the paths to the application controllers
			self::map('/application/controllers', $paths);

			// Then retrieve the paths to the various modules' controllers
			foreach ( Kennel::$MODULES as $module )
				self::map("/modules/{$module}/controllers", $paths);

			// And finally retrieve the system controllers
			self::map('/system/controllers', $paths);

			// See Controller::compare()
			uksort($paths, array('Controller', 'compare'));

			return $paths;
		}

		/**
		 * The route to a main action with parameters will conflict with the other
		 * controllers in the same directory, eg:
		 *   /:slug
		 *   /controller
		 * At this point we already enforced cascading by ignoring overriden
		 * controllers in the Controller::map() method, so we shift the positions in
		 * the resulting array to make sure the more specific routes will be
		 * registered first.
		 * 
		 * We accomplish that with a basic score system: each slash adds two points,
		 * if "/main" is present in the reference we subtract one point. This way
		 * we make sure nested controller references come first, and the main
		 * controller comes last for each directory.
		 **/
		private static function compare($a, $b)
		{
			$sa = substr_count($a, '/') * 2;
			if ( preg_match('/\/?main/', $a) ) $sa--;

			$sb = substr_count($b, '/') * 2;
			if ( preg_match('/\/?main/', $b) ) $sb--;

			if ( $sa > $sb ) return -1;
			elseif ( $sa < $sb ) return 1;
			else return 0;
		}

		/**
		 * Recursivelly scan the controller $root for controller files, populating
		 * the &$paths array with the controller references as keys and their paths
		 * as values. If a reference is already in &$path it is not overriden, so
		 * this method should be called in the cascading priority order of
		 * application > modules > system.
		 *
		 * @param string $root The root path to search for controllers. This should
		 * be a bundle /controllers directory.
		 * @param array &$paths The array to populate
		 * @param string $relative_path Optional. The controller directory path
		 * relative to it's root.
		 * @return void
		 **/
		private static function map($root, &$paths, $relative_path='')
		{
			$dir = Kennel::$ROOT_PATH . $root . ($relative_path ? '/' . $relative_path : '');
			$files = array_values( array_diff( scandir($dir), array('.', '..') ) );

			foreach ($files as $file)
			{
				$filepath = $dir . '/' . $file;
				$pathinfo = pathinfo($filepath);

				if ( is_dir($filepath) )
				{
					self::map($root, $paths, ltrim($relative_path.'/'.$file, '/'));
				}
				elseif ( $pathinfo['extension'] === 'php' )
				{
					$controller = ltrim($relative_path . '/' . str_replace('_', '-', $pathinfo['filename']), '/');

					// By loading in the cascading order (in the Controller::paths()
					// method) and not overriding the $paths keys, we apply the cascading
					// rules to the automatic Controller routing generation.
					if ( !isset($paths[$controller]) )
						$paths[$controller] = self::class_name($controller);
				}
			}
		}
		
	}
	
?>
