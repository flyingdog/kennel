<?php
	class Criteria
	{
		// Comparison Operators
		const EQUAL = '=';
		const NOT_EQUAL = '!=';
		const LIKE = 'LIKE';
		const IN = 'IN';
		const NOT_IN = 'NOT IN';
		const GREATER_THAN = '>';
		const LESS_THAN = '<';
		const GREATER_EQUAL = '>=';
		const LESS_EQUAL = '<=';
		const MATCH = 'MATCH';

		// Logical operators
		const L_NONE = NULL;
		const L_AND = 'AND';
		const L_OR = 'OR';
		
		// Joins
		const LEFT_JOIN = 'LEFT JOIN';
		const RIGHT_JOIN = 'RIGHT JOIN';
		const INNER_JOIN = 'INNER JOIN';
		
		// Values
		const NOW = 'NOW()';
		const IS_NULL = 'IS NULL';
		const NULL = 'IS NULL'; //alias
		const IS_NOT_NULL = 'IS NOT NULL';
		const NOT_NULL = 'IS NOT NULL'; //alias
		
		// Query variables
		var $criterion_groups = array();
		var $match_columns;
		var $from_model_name;
		var $joins = array();
		var $order_by = array();
		var $group_by = array();
		var $limit;
		var $offset = 0;
		var $exclusions = array();
		
		public function __construct($model_name=null)
		{
			$this->from_model_name = $model_name;
		}
		
		/**
		 * Create a new Criterion and add it to the first parentheses-grouped set
		 * of criteria in the query. If no groups were added yet, this method
		 * creates one.
		 *
		 * @return void
		 * @author 
		 **/
		public function add($column, $value, $operator=Criteria::EQUAL, $logical=Criteria::L_AND)
		{
			$criterion = new Criterion($column, $value, $operator, $logical);

			if ( count($this->criterion_groups) === 0 )
				$this->criterion_groups[0] = new CriterionGroup(Criteria::L_NONE);

			$this->criterion_groups[0]->add($criterion);
			return $this;
		}
		
		public function limit($limit, $offset=null)
		{
			$this->limit = $limit;
			if (isset($offset)) $this->offset = $offset;
			return $this;
		}
		
		public function offset($offset)
		{
			$this->offset = $offset;
			return $this;
		}
		
		public function addAscendingOrder($column)
		{
			$this->order_by[] = array('column'=>$column, 'direction'=>'ASC');
			return $this;
		}
		public function orderAsc($column) { return $this->addAscendingOrder($column); } // Alias
		
		public function addDescendingOrder($column)
		{
			$this->order_by[] = array('column'=>$column, 'direction'=>'DESC');
			return $this;
		}
		public function orderDesc($column) { return $this->addDescendingOrder($column); } // Alias
		
		public function addRandomOrder()
		{
			$this->order_by[] = array('direction'=>'RAND');
			return $this;
		}
		
		public function addGroupBy($column)
		{
			$this->group_by[] = $column;
			return $this;
		}
		public function group($column) { return $this->addGroupBy($column); } // Alias
		
		public function setFrom($model_name)
		{
			$this->from_model_name = $model_name;
			return $this;
		}
		
		/**
		 * Exclude an alias from eager-loading
		 *
		 **/
		public function exclude($alias)
		{
			$this->exclusions[] = $alias;
			return $this;
		}

		// TODO: the "$pre_select_criteria" has an awful name, it's just a Criteria
		// to use as a nested select to join with. In addition, the Criteria should
		// be passed instead of the model name, and the 4th argument should be the
		// alias.
		public function addJoin($model_name, $on, $join_type=self::INNER_JOIN, Criteria $pre_select_criteria=null)
		{
			// Legacy addJoin used a different method signature that allowed a single
			// comparison for the ON statement. Checking for an array as the 2nd
			// parameter helps debugging legacy applications.
			if ( !is_array($on) )
				debug::error("Criteria error: <code>\$on</code> must be an array, "
					. gettype($on) . ' passed.');

			$join = array( 'on' => $on, 'join_type' => $join_type );

			// Model name can be a simple string, or an associative array with the
			// name and an alias.
			if ( is_array($model_name) )
			{
				$join['model_name'] = $model_name['model_name'];
				$join['alias'] = $model_name['alias'];
			}
			else
			{
				$join['model_name'] = $join['alias'] = $model_name;
			}

			// Joins can use a criteria object that is used to filter the table prior to the join proper, via nested
			// select. This can significantly improve performance in some cases.
			// Example output: (...) LEFT JOIN (SELECT * FROM artists WHERE MATCH(name) AGAINST('+SOMETHING')) as tags (...)
			if ( $pre_select_criteria )
				$join['pre_select_criteria'] = $pre_select_criteria;

			// Don't add the exact same join twice.
			foreach ( $this->joins as $previous )
				if ( $previous === $join )
					return;

			$this->joins[] = $join;
			return $this;
		}

		// Convenience methods
		public function innerJoin($model_name, $on, Criteria $pre_select_criteria=null) {
			return $this->addJoin($model_name, $on, self::INNER_JOIN, $pre_select_criteria);
		}
		public function rightJoin($model_name, $on, Criteria $pre_select_criteria=null) {
			return $this->addJoin($model_name, $on, self::RIGHT_JOIN, $pre_select_criteria);
		}
		public function leftJoin($model_name, $on, Criteria $pre_select_criteria=null) {
			return $this->addJoin($model_name, $on, self::LEFT_JOIN, $pre_select_criteria);
		}

		/**
		 * Adds a parentheses-grouped set of criteria without any preceding logical
		 * operators.
		 *
		 * @param Criterion ...
		 * @return void
		 **/
		public function addGroup()
		{
			$group = new CriterionGroup(Criteria::L_NONE);

			$criteria = func_get_args();
			foreach ($criteria as $criterion)
			{
				$group->add($criterion);
			}
			$this->criterion_groups[] = $group;
			return $this;
		}
		
		/**
		 * Adds a parentheses-grouped set of criteria with a preceding AND logical
		 * operator
		 *
		 * @param Criterion ...
		 * @return void
		 **/
		public function andGroup()
		{
			$group = new CriterionGroup(Criteria::L_AND);

			$criteria = func_get_args();
			foreach ($criteria as $criterion)
			{
				$group->add($criterion);
			}
			$this->criterion_groups[] = $group;
			return $this;
		}

		/**
		 * Adds a parentheses-grouped set of criteria with a preceding OR logical
		 * operator
		 *
		 * @param Criterion ...
		 * @return void
		 **/
		public function orGroup()
		{
			$group = new CriterionGroup(Criteria::L_OR);

			$criteria = func_get_args();
			foreach ($criteria as $criterion)
			{
				$group->add($criterion);
			}
			$this->criterion_groups[] = $group;
			return $this;
		}
		
		/**
		 * Instantialize and return a Criterion that can be added to a Criteria
		 * grouping. When added at the first position in a group, it won't be
		 * preceded by any logical operators, otherwise will be preceded by a 
		 * logical AND.
		 *
		 * @param string $field
		 * @param mixed $value
		 * @param const $operator the comparison operator
		 * @return Criterion
		 **/
		public function get($field, $value, $operator=Criteria::EQUAL)
		{
			return new Criterion($field, $value, $operator, Criteria::L_AND);
		}

		/**
		 * Instantialize and return a Criterion that can be added to a Criteria
		 * grouping. When added at the first position in a group, it won't be
		 * preceded by any logical operators, otherwise will be preceded by a 
		 * logical OR.
		 *
		 * @param string $field
		 * @param mixed $value
		 * @param const $operator the comparison operator
		 * @return Criterion
		 **/
		public function getOr($field, $value, $operator=Criteria::EQUAL)
		{
			return new Criterion($field, $value, $operator, Criteria::L_OR);
		}

		/**
		 * Shortcut for `ORM::retrieve($criteria)`. Performs the database retrieval
		 * with this criteria instance and return the result.
		 *
		 * @return Array
		 **/
		public function retrieve() {
			return ORM::retrieve($this);
		}

		/**
		 * Shortcut for `ORM::retrieveFirst($criteria)`. Limits the criteria
		 * retrieval to one record, performs the database retrieval and return the
		 * result.
		 *
		 * @return Model or null
		 **/
		public function retrieveFirst() {
			return ORM::retrieveFirst($this);
		}

		/**
		 * Shortcut for `ORM::count($criteria)`. Returns the number of entries
		 * matching the criteria.
		 *
		 * @return int
		 **/
		public function count() {
			return ORM::count($this);
		}
		
	}
	
?>
