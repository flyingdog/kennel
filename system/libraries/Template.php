<?php
	/**
		*  TODO i18n
		*/
	class Template extends View
	{
		// Static variables
		const DOCTYPE_HTML5 = '<!doctype html>';
		const MODERNIZR = 'modermozr-1.5.min.js';
		const PNGFIX = 'dd_belatedpng-0.0.8a.min.js';
		const RAPHAEL = 'raphael-min.js';
		const JQUERY = 'jquery-1.9.1.min.js';
		const BOOTSTRAP = 'bootstrap.min.css';
		
		static $INSTANCE = null;
		
		// Variables
		var $lang = '';
		var $dir = 'ltr';
		var $favicon = null;
		var $title = null;
		var $titleSeparator = '-';
		var $bodyClass = null;
		var $bodyId = null;
		var $ngApp = null;
		var $base = null;
		
		// Structure
		private $_html;
		private $_head;
		private $_body;
		
		// Meta
		private $_meta = array();
		
		// Links
		private $_links = array();
		
		// Resources
		private $_stylesheets = array();
		private $_scripts = array();
		private $_footScripts = array();
		
		// Template View
		private $_template;
		
		function __construct($template)
		{
			$this->_template = new View($template, $this);
			self::$INSTANCE = $this;
		}
		
		static function getInstance($view_name=null)
		{
			if (!self::$INSTANCE)
			{
				if ($view_name) self::$INSTANCE = new self($view_name);
				else return debug::error('Template::getInstance(null): no instance created yet');
			}
			return self::$INSTANCE;
		}
		
		// Ovewriting normal View behavior
		//////////////////////////////////
		
		private function _getOutput()
		{
			// Make $template and $title accessible in the descendant views
			$this->__set('template', $this);
			$this->__set('title', $this->title);
			
			// Template View (must run first since child views may call functions
			// from $template)
			$templateView = XML::text($this->_template->__toString());
			// TODO: review this
			
			// <html>
			$this->_html = XML::element('html');
			$this->_html->dir =$this->dir;
			if (Kennel::getSetting('i18n', 'enabled'))
				$this->_html->lang = i18n::getLang();
			
			// <head>
			$this->_head = XML::element('head', $this->_html);
			
			// <title>
			$title = XML::element('title', $this->_head);
			$title->setValue($this->getTitle());
			
			// favicon
			if ($this->favicon)
				$this->_head->adopt(html::favicon($this->favicon));
			
			// Content Type
			$this->_head->adopt(html::meta(array( 'charset'=>'utf-8' )));

			// Base
			if ($this->base) {
				$base = XML::element('base', $this->_head, $this->base);
				$base->self_closing = XML::SELF_CLOSING_HTML;
			}
			
			// <meta>
			foreach ($this->_meta as $meta)
				$this->_head->adopt(html::meta($meta));
			
			// <link>
			foreach ($this->_links as $link)
				$this->_head->adopt(html::link($link['rel'], $link['href'], $link['type'], $link['title']));
			
			// Header <style>
			$this->_head->adopt(html::css($this->_stylesheets));

			// HTML5 Shiv
			XML::text('<!--[if lt IE 9]><script src="system/assets/js/html5shiv.js"></script><![endif]-->', $this->_head);
			
			// Header <script>
			$this->_head->adopt(html::js($this->_scripts));
			
			// <body>
			$this->_body = XML::element('body', $this->_html);
			$this->_body->class = browser::css();
			
			if (Kennel::getSetting('i18n', 'enabled'))
			$this->_body->class .= ' ' . i18n::getLang();
			if ($this->bodyClass) $this->_body->class .= ' ' . $this->bodyClass;
			if ($this->bodyId) $this->_body->id = $this->bodyId;
			if ($this->ngApp) $this->_body->set('ng-app', $this->ngApp);
			
			// Inject the Template View
			$this->_body->adopt($templateView);
			
			// Foot <script>
			$this->_body->adopt(html::js($this->_footScripts));
			
			// Return the whole shebang
			return self::DOCTYPE_HTML5 . $this->_html->output(true);
		}
		
		function render()
		{
			header('content-type: text/html; charset:UTF-8');
			print $this->_getOutput();
		}
		
		// Aditional utility methods
		////////////////////////////
		
		function getTitle()
		{
			if ($this->title && Kennel::getSetting('application', 'title'))
				return $this->title . " {$this->titleSeparator} " . Kennel::getSetting('application', 'title');
			elseif (!$this->title && Kennel::getSetting('application', 'title'))
				return Kennel::getSetting('application', 'title');
			elseif ($this->title && !Kennel::getSetting('application', 'title'))
				return $this->title;
			else
				return 'Untitled';
		}
		
		function meta($name_or_properties, $content=null)
		{
			if ( is_array($name_or_properties) ) {
				$this->_meta[] = $name_or_properties;
			} else {
				$this->_meta[] = array('name'=>$name_or_properties, 'content'=>esc::attr($content));
			}
		}
		
		function link($rel, $type, $href, $title=null)
		{
			$this->_links[] = array('rel'=>$rel, 'type'=>$type, 'href'=>$href, 'title'=>$title);
		}
		
		function css()
		{
			$arguments = func_get_args();
			foreach ($arguments as $stylesheet)
				if (!in_array($stylesheet, $this->_stylesheets))
					$this->_stylesheets[] = $stylesheet;
		}
		
		function js()
		{
			$arguments = func_get_args();
			$lastArg = $arguments[count($arguments)-1];
			if (is_bool($lastArg) && $lastArg === true) {
				array_pop($arguments);
				foreach ($arguments as $script)
					if (!in_array($script, $this->_scripts))
						$this->_footScripts[] = $script;
				} else {
					foreach ($arguments as $script)
					if (!in_array($script, $this->_scripts))
						$this->_scripts[] = $script;
			}
		}

	}
?>