<?php
/*
'It  is  a long tail, certainly said Alice, looking down
with wonder at the Mouse's tail; 'but why do you call it
sad?'  And  she  kept  on  puzzling  about  it while the
Mouse was speaking, so that her idea  of  the  tale  was
something like this:- 'Fury said to a
                   mouse, That
                     he met in the
                          house, "Let
                              us both go
                                 to law: I
                                 will prosec-
                                  cute you --
                                Come, I'll
                             take no de-
                         nial:  We
                     must  have
                 the trial;
               For really
             this morn-
           ing I've
          nothing
         to do."
          Said the
           mouse to
            the cur,
             "Such   a
               trial, dear
                   sir, With
                      no  jury
                        or judge,
                            would
                         be wast-
                      ing our
                   breath."
                 "I'll be
               judge,
            I'll be
          jury,"
         said
        cun-
        ning
          old
           Fury;
             "I'll
               try
                 the
                  whole
                   cause,
                    and
                  con-
                demn
              you to
           death"
*/

	class Model
	{
		private static $_COUNTER = 0;
		protected $_uid;

		private static $_DB;
		protected $_data = array();
		protected $_synced_data = array();
		private $_associations = array();
		private $_assoc_status = array(); // loading status, boolean
		private $_edited = false;
		private $_deleted = false;

		/**
		 * The Schema instance this model is based on
		 *
		 * @var string
		 **/
		var $schema;

		/**
		 * The model name
		 *
		 * @var string
		 **/
		var $model_name;
		
		/**
		 * Fields with validation issues from the latest validate() or save() call;
		 * further calls reset this.
		 *
		 * @var array
		 **/
		public $invalid_fields = array();
		
		/**
		 * The constructor takes in the $model_name, instantiates the Schema and
		 * sets the initial state for the Model instance.
		 *
		 * @param string $model_name
		 * @return void
		 **/
		public function __construct($model_name)
		{
			$this->_uid = ++self::$_COUNTER;
			$this->model_name = $model_name;
			$this->schema = ORM::schema($this->model_name);
			foreach ( $this->schema->fields as $field )
			{
				$this->_data[$field->name] = null;
				$this->_synced_data[$field->name] = null;
			}
		}

		/**
		 * Polymorphic associations - associations where a child model can be
		 * associated by multiple parent models, or yet multiple relationships with
		 * the same parent model - work by storing the parent `id` as regular
		 * relationships as well as the parent `model` and the name of the
		 * relationship used in the association.
		 *
		 * Because we need to know what specific relationship is being used to
		 * associate the two models, a simple `parent.relationship = child` works
		 * but `child.relationship = parent` becomes impossible, since the child
		 * relationship is abstract.
		 * 
		 * This method allows just such association, by receiving the child
		 * relationship $name, the $parent to be associated and that model's
		 * relationship name.
		 *
		 * @return void
		 * @author 
		 **/
		public function assoc($relationship, $parent, $inverse_relationship)
		{
			if ( $this->_deleted ) $this->throwDeletedWarning(__METHOD__);

			if ( !array_key_exists($relationship, $this->schema->relationships) )
				debug::error("Model error: relationship {$this->model_name}.{$relationship}
					does not exist.");

			$rel = $this->schema->relationships[$relationship];

			// Check for a polymorphic BELONGS_TO
			if ( !$rel->polymorphic || $rel->type !== Relationship::BELONGS_TO )
				debug::error('Model::assoc must only be used to associate a polymorphic
				belongs-to relationship.');

			// Validate the parent abstract model
			if ( $parent->schema->relationships[$inverse_relationship]->as !== $rel->destination )
				debug::error("Model error: `{$parent->model_name}` cannot be assocaited
					to `{$this->model_name}.{$relationship}` because it is not a
					`{$this->destination}`");

			// Model is already associated
			if ( isset($this->_associations[$rel->name]) && $this->_associations[$rel->name] == $parent )
				return $parent;

			// Model association
			$this->_associations[$rel->name] = $parent;

			// Primary key assignment
			$this->{"{$rel->destination}_id"} = $parent->id;
			$this->{"{$rel->destination}_model"} = $parent->model_name;
			$this->{"{$rel->destination}_rel"} = $inverse_relationship;

			// Inverse association
			foreach ( $parent->schema->relationships as $inverse )
			{
				if ( $inverse->name === $inverse_relationship )
				{
					if ( $inverse->type === Relationship::HAS_ONE )
						$parent->{$inverse_relationship} = $this;
					else if ( $inverse->type === Relationship::HAS_ONE )
						$parent->{$inverse_relationship}->add($this);
				}
			}


			return $parent;
		}

		/**
		 * Magic method to dinamically assign values to fields and associate model
		 * relationships.
		 *
		 * @param string $name
		 * @param mixed $value
		 * @return mixed
		 **/
		public function __set($name, $value)
		{	
			if ( $this->_deleted ) return $this->throwDeletedWarning(__METHOD__);

			// Model dissociation
			if ( $value === NULL
				&& array_key_exists($name, $this->schema->relationships) )
			{
				if ( array_key_exists($name, $this->_associations) ) unset($this->_associations[$name]);
				return NULL;
			}

			// Model association
			if ( is_a($value, 'Model')
				&& array_key_exists($name, $this->schema->relationships) )
			{
				$rel = $this->schema->relationships[$name];

				if ( $value->model_name !== $rel->destination )
					debug::error("Model error: cannot associate `{$value->model_name}` to
						`{$this->model_name}.{$name}`");

				// Model is already associated
				if ( isset($this->_associations[$rel->name]) && $this->{$rel->name} == $value )
					return $value;

				switch ( $rel->type )
				{
					case Relationship::HAS_ONE:

						// Model association
						$this->_associations[$name] = $value;

						// Foreign key assignment
						$this->assign_foreign_key($rel, $value);

						// Inverse association
						if ( !$rel->polymorphic )
						{
							foreach ( $value->schema->relationships as $inverse )
								if ( $inverse->destination === $this->model_name )
									$value->{$inverse->name} = $this;
						}
						else
						{
							$value->assoc($rel->as, $this, $rel->name);
						}

						break;
					case Relationship::BELONGS_TO:
						if ( $rel->polymorphic )
						{
							debug::error("Model error: cannot associate the belongs-to
								<code>\${$this->model_name}->{$rel->name}</code> relationship to
								<code>\${$value->model_name}</code> because <br>
								the relationship name cannot be asserted. Use
								<code>\${$this->model_name}->assoc('{$rel->name}', \${$value->model_name}, '{relationship}')</code>
								<br> or the inverse association
								<code>\${$value->model_name}->{relationship} = \${$this->model_name}.</code>
								instead.");
						}
						else
						{
							// Model association
							$this->_associations[$name] = $value;

							// Foreign key assignment
							$this->{($rel->polymorphic ? $rel->as : $rel->destination).'_id'} = $value->id;

							// Inverse association
							foreach ( $value->schema->relationships as $inverse )
							{
								if ( $inverse->destination === $this->model_name )
								{
									if ( $inverse->type === Relationship::HAS_ONE )
										$value->{$inverse->name} = $this;
									else if ( $inverse->type === Relationship::HAS_MANY )
										$value->{$inverse->name}->add($this);
								}
							}

						}
						break;
					case Relationship::HAS_MANY:
						debug::error("Model error: cannot associate the has-many
							<code>\${$this->model_name}->{$rel->name}</code> relationship to
							<code>\${$value->model_name}</code>.<br>
							Use <code>\${$this->model_name}->{$rel->name}->add(\${$value->model_name})</code>
							instead.");
				}

				return $value;
			}
			
			// Value assignment
			else if ( array_key_exists($name, $this->schema->fields) )
			{
				// Cast the value to the appropriate scalar type
				$value = $this->schema->$name->cast($value);

				// Call the setter from the model class, if defined. The value returned
				// is then stored.
				$set_method = "set_{$name}";
				if ( method_exists($this, $set_method) )
					$value = $this->$set_method($value);

				// If the new value is different than the previous, we set the new
				// value and flag the instance as edited.
				if ( $this->_data[$name] !== $value )
				{
					$this->_data[$name] = $value;
					$this->_edited = true;

					$pk = $this->schema->primary_key()->name;

					// If updating a primary key, update the child references too.
					if ( $pk === $name )
					{
						// Don't allow changing the primary key if it was already stored in
						// the database.
						if ( $this->_synced_data[$pk] )	
							debug::error("Model error: primary key changes are not allowed;
								tried setting `{$this->model_name}.{$pk}` to {$value}.", 2);

						foreach ( $this->_associations as $key => $model )
							$this->assign_foreign_key($this->schema->relationships[$key], $model);
					}
				}

				return $value;
			}
			
			debug::error("Model error: trying to set undefined field or relationship
				`{$this->model_name}.{$name}`", array(
					'model' => $this->model_name,
					'name' => $name,
					'value' => $value,
					'schema' => $this->schema->dump(true)
				 ));
		}

		/**
		 * has-one and has-many associations are stored in the child's table. This
		 * method assigns the appropriate references to the child model, including
		 * the polymorphic references.
		 *
		 * @return void
		 * @author 
		 **/
		public function assign_foreign_key($rel, $child)
		{
			if ( $this->_deleted ) return $this->throwDeletedWarning(__METHOD__);

			// Doesn't apply to belongs-to relationships
			if ( $rel->type === Relationship::BELONGS_TO ) return;

			// Foreign key assignment
			if ( $rel->polymorphic )
			{
				$child->{"{$rel->as}_id"} = $this->id;
				$child->{"{$rel->as}_model"} = $this->model_name;
				$child->{"{$rel->as}_rel"} = $rel->name;
			}
			else
			{
				$child->{"{$rel->origin}_id"} = $this->id;
			}
		}
		
		/**
		 * Magic method to dynamically get property values and relationships.
		 *
		 * @param string $name
		 * @return mixed
		 **/
		public function __get($name)
		{
			if ( $this->_deleted ) return $this->throwDeletedWarning(__METHOD__);

			// Accessing an association
			foreach ( $this->schema->relationships as $rel )
			{
				// The keyword is either the relationship name or the abstract
				// entity for polymorphic associations
				if ( $rel->name === $name or $rel->as === $name )
				{
					if ( array_key_exists($name, $this->_associations) )
					{
						if ( $this->_associations[$name] && $this->_associations[$name]->isValid() )
							return $this->_associations[$name];
						else
							return $this->_associations[$name] = null;
					}
					else
					{
						// Lazy loading
						switch ( $rel->type )
						{
							case Relationship::HAS_MANY:
								$this->_associations[$name] = new ModelSet($this, $rel);
								break;
							case Relationship::HAS_ONE:
							case Relationship::BELONGS_TO:
								$this->_associations[$name] = $this->lazyLoad($name);
						}
						$this->_assoc_status[$rel->name] = true;
						return $this->_associations[$name];
					}
				}
			}
			
			// Field. If there is a getter with the signature
			// ``getField_name($name)`` declared in the model class, that method is
			// called and it's value is returned.
			if ( array_key_exists($name, $this->_data) )
			{
				$get_method = 'get' . ucfirst(strtolower($name));
				if ( method_exists($this, $get_method) )
					return $this->$get_method($this->_data[$name]);
				else
					return $this->_data[$name];
			}
			
			debug::error("Model error: trying to get undefined field or relationship
				`{$this->model_name}.{$name}`", array( 'schema' => $this->schema->dump(true) ), 1);
		}

		/**
		 * undocumented function
		 *
		 * @return void
		 * @author 
		 **/
		private function lazyLoad($name)
		{
			$rel = $this->schema->relationships[$name];

			$this->_assoc_status[$rel->name] = true;

			switch ( $rel->type )
			{
				case Relationship::HAS_ONE:

					// Can't load children if this instance was not assigned an id
					if ( !$this->id ) return null;

					$c = new Criteria($rel->destination);
					if ( $rel->polymorphic )
					{
						$c->add("{$rel->as}_id", $this->id);
						$c->add("{$rel->as}_model", $this->model_name);
						$c->add("{$rel->as}_rel", $rel->name);
						$model = ORM::retrieveFirst($c);

						// Relationship is null
						if( !$model ) return null;

						// Inverse association
						$model->assoc($rel->as, $this, $rel->name);

						return $model;

					}
					else
					{
						$c->add("{$this->model_name}_id", $this->id);
						$model = ORM::retrieveFirst($c);

						// Relationship is null
						if ( !$model ) return null;

						// Inverse association
						if ( $model )
							foreach ( $model->schema->relationships as $inverse )
								if ( $inverse->destination == $this->model_name )
									$model->{$inverse->name} = $this;

						return $model;
					}


				case Relationship::BELONGS_TO:
					if ( $rel->polymorphic )
					{
						// Can't load parent if this instance was not assigned the foreign
						// references
						if ( !$this->{"{$rel->destination}_id"} ) return null;

						$c = new Criteria($this->{"{$rel->destination}_model"});
						$c->add('id', $this->{"{$rel->destination}_id"});
						$c->exclude($this->{"{$rel->destination}_rel"});

						$model = ORM::retrieveFirst($c);

						// Inverse association
						$inverse_relationship = $this->{"{$rel->destination}_rel"};
						$model->{$inverse_relationship} = $this;

						return $model;
					}
					else
					{
						// Can't load parent if this instance was not assigned the foreign
						// key
						if ( !$this->{"{$rel->name}_id"} ) return null;

						$schema = ORM::schema($rel->destination);
						$c = new Criteria($rel->destination);
						$c->add('id', $this->{"{$rel->name}_id"});
						
						foreach ( $schema->relationships as $inverse )
							if ( $inverse->destination === $this->model_name )
							{
								$c->exclude($inverse->name);
								break;
							}

						$model = ORM::retrieveFirst($c);

						// Inverse association
						// switch ( $inverse->type )
						// {
						// 	case Relationship::HAS_ONE:
						// 		$model->{$inverse->name} = $this;
						// 		break;
						// 	case Relationship::HAS_MANY:
						// 		$model->{$inverse->name}->add($this, false);
						// }
						// TODO: This hangs in $article->author

						return $model;
					}
			}
		}

		// TODO
		// function __isset($name) { }
		// function __unset($name) { }
		
		/**
		 * Returns the validity of this model instance. Currently the only invalid
		 * state is after the model was deleted, but still has a reference pointing
		 * to it.
		 *
		 * @return boolean
		 **/
		public function isValid()
		{
			return !$this->_deleted;
		}

		/**
		 * Magic method that returns the model name when trying to print out the
		 * variable holding the Model instance.
		 *
		 * @return string
		 **/
		public function __toString()
		{
			if ( $this->_deleted ) return $this->throwDeletedWarning(__METHOD__);
			return $this->model_name;
		}
		
		/**
		 * This method assigns a $value to a $field after casting it to the
		 * appropriate scalar type, but also sets the internal synced data to that
		 * value, doesn't propagate changes to the primary key and don't cause the
		 * model state to turn unsynced. It is intended to populate the model with
		 * values fresh off the database, and is the method used by the ORM class
		 * to do so.
		 *
		 * @param string $name
		 * @param string $value
		 **/
		public function hydrate($name, $value)
		{
			if ( $this->_deleted ) return $this->throwDeletedWarning(__METHOD__);

			$value = $this->schema->$name->cast($value);

			$this->_data[$name] = ($value !== null) ? $value : null;
			$this->_synced_data[$name] = $this->_data[$name];

			$this->_edited = false;
		}

		/**
		 * Set the model field values to matching content submitted via POST
		 * request, sanitized by the input helper.
		 *
		 * @return void
		 **/
		function fromPost()
		{
			if ( $this->_deleted ) return $this->throwDeletedWarning(__METHOD__);

			foreach ( $this->schema->fields as $field )
			{
				$field_name = $field->name;
				$this->$field_name = input::post($field_name);
			}
		}
		
		/**
		 * Delete the entry from the database as well as all it's children.
		 *
		 * @return void
		 **/
		function delete()
		{
			if ( $this->_deleted ) return $this->throwDeletedWarning(__METHOD__);

			// foreach ( $this->schema->relationships as $key => $rel )
			// {
			// 	// We use the getter deliberately. This way we lazy load associations
			// 	// as necessary.
			// 	if ( $assoc = $this->{$rel->name} )
			// 	{
			// 		// Delete only children associations.
			// 		if ( $rel->type === Relationship::HAS_ONE
			// 			|| $rel->type === Relationship::HAS_MANY )
			// 		{
			// 			// ModelSets also have a delete() method, which deletes all
			// 			// models from the set.
			// 			$assoc->delete();
			// 		}
			// 	}
			// }

			// Delete database entry
			$c = new Criteria($this->model_name);
			foreach ( $this->schema->fields as $field )
				$c->add($field->name, $this->_data[$field->name]);

			$this->_deleted = true;

			return ORM::delete($c);
		}

		// TODO: documenet method
		function validate()
		{
			if ( $this->_deleted ) return $this->throwDeletedWarning(__METHOD__);

			$this->invalid_fields = array();
			$uniques = array();
			
			foreach ( $this->schema->fields as $field )
			{
				$numeric_fields = array( 'int', 'tinyint', 'bigint', 'float', 'double',
					'decimal' );
				$field_is_numeric = in_array($field->type, $numeric_fields);
				
				// Simple required field validation
				if ( (
						( !$field_is_numeric && $this->_data[$field->name] === NULL )
						|| ( $field_is_numeric && !is_numeric($this->_data[$field->name] ) )
					)
					&& $field->required && !$field->primaryKey
				)
				{
					$this->invalid_fields[$field->name][] = __('This field is required');
				}
				
				// Gather unique fields
				if ( $field->unique && !$field->primaryKey )
					$uniques[] = $field->name;
			}
			
			// Unique field validation only for new model instances
			if ( !$this->id )
				// Loop through each unique field and retrieve instances;
				// Could be refactored to reduce the number of queries made
				foreach ( $uniques as $field_name )
				{
					$c = new Criteria($this->model_name);
					$c->add($field_name, $this->_data[$field_name]);
					if (ORM::count($c) > 0)
					{
						$this->invalid_fields[$field_name][] = __('Already registered');
					}
				}
			
			if ( count($this->invalid_fields) > 0 ) return false;
			else return true;
		}
		
		/**
		 * Saves the current contents of the model to the database, using the
		 * appropriate INSERT or UPDATE statement. If $recursive is true (default)
		 * also saves all the associations this instance owns.
		 *
		 * @return void
		 * @author 
		 **/
		public function save($recursive=true, $recursion_stack=array())
		{
			//error_log("saving {$this->model_name}_{$this->_uid}.{$this->id}");
			if ( $this->_deleted ) return $this->throwDeletedWarning(__METHOD__);

			$recursion_stack[] = $this;

			// Validate the model before saving
			if ( !$this->validate() ) {
				$this->dump();
				debug::error("Trying to save invalid content for model {$this->model_name}");
				return false;
			}


			// Retrieve the primary key for this model
			$primaryKey = $this->schema->primary_key()->name;
			
			// No need to save if the model has a PK set and is not edited
			if ($this->$primaryKey && !$this->_edited)
				return null;
			// TODO: flag model as edited after a successful association, otherwise
			// the recursive save will fail for has-one/has-many associations where
			// no change was made to the parent model data.
			
			$sql = $this->save_query();
			if (!self::$_DB) self::$_DB = new MySQL;
			self::$_DB->query($sql);
			
			// Update the primary key
			$insert_id = self::$_DB->insertId();
			if ( $insert_id )
				$this->id = $insert_id;
			
			// Update the sync information
			$this->_synced_data = $this->_data;
			$this->_edited = false;

			// Recursivelly save the current relationships

			// if ( $recursive )
			// 	foreach ( $this->_associations as $name => $assoc )
			// 	{
			// 		if ( !in_array($assoc, $recursion_stack) )
			// 		{
			// 			if ( $assoc->isValid() )
			// 				$assoc->save(true, $recursion_stack);
			// 			else
			// 				// As good a time to remove the assoc as any
			// 				unset($this->_associations[$name]);
			// 		}
			// 	}

			return true;
		}
		
		/**
		 * Returns the SQL query to save the contents of the model to the database.
		 * Uses the INSERT statement for new instances or UPDATE for existing ones.
		 *
		 * @return void
		 * @author 
		 **/
		public function save_query()
		{
			if ( $this->_deleted ) return $this->throwDeletedWarning(__METHOD__);

			$primaryKey = $this->schema->primary_key();
			
			$columns = array();
			$newValues = array();
			$syncedValues = array();
			
			// Add quotes for non-numeric field values, used to the SQL statement
			foreach ( $this->schema->fields as $field )
			{
				$columns[] = '`' . $field->name . '`';
				switch (strtolower($field->type))
				{
					case 'varchar':
					case 'char':
					case 'text':
					case 'datetime':
					case 'date':
					case 'time':
						
						if ( $this->_data[$field->name] !== NULL )
							$newValues[] = '"'
							. MySQL::escape($this->_data[$field->name]) . '"';
						else
							$newValues[] = 'NULL';
							
						if ( $this->_synced_data[$field->name] !== NULL )
							$syncedValues[] = '"'
							. MySQL::escape($this->_synced_data[$field->name]) . '"';
						else
							$syncedValues[] = 'NULL';
							
						break;
						
					case 'int':
					case 'tinyint':
					case 'bigint':
					case 'float':
					case 'double':
					case 'decimal':
						
						if ( $this->_data[$field->name] !== NULL
							&& is_numeric($this->_data[$field->name]) )
							$newValues[] = $this->_data[$field->name];
						else
							$newValues[] = 'NULL';
						
						if ( $this->_synced_data[$field->name] !== NULL
							&& is_numeric($this->_synced_data[$field->name]) )
							$syncedValues[] = $this->_synced_data[$field->name];
						else
							$syncedValues[] = 'NULL';
						
						break;
						
					default:
						debug::error("Model::save - Unsuported field type
							\"{$field->type}\" for field \"{$field->name}\" on model
							\"{$this->model_name}\"");
				}
			}
			
			// Check if it's an existing record, build SQL statement accordingly
			if ( !$this->_synced_data[$primaryKey->name] )
			{
				$sql = "INSERT INTO `{$this->schema->table}` (";
				$sql .= implode(', ', $columns);
				$sql .= ")\nVALUES (";
				$sql .= implode(', ', $newValues);
				$sql .= ");";
			} else
			{
				$dataList = array();
				foreach ( $columns as $key => $column )
					$dataList[] = "\n {$column} = {$newValues[$key]}";
				
				$c = new Criteria($this->model_name);
				foreach ( $this->schema->fields as $field )
					$c->add($field->name, $this->_synced_data[$field->name]);
				
				$sql = "UPDATE `{$this->schema->table}` as `{$this->model_name}` SET " . implode(', ', $dataList);
				$sql .= "\nWHERE " . ORM::whereString($c) . ';';
			}
			
			return $sql;
		}
		
		/**
		 * Returns the current field values from the model as an associative array.
		 *
		 * @return array
		 **/
		public function toArray()
		{
			if ( $this->_deleted ) return $this->throwDeletedWarning(__METHOD__);

			return $this->_data;
		}

		/**
		 * Returns the current field values from the model as a JSON string.
		 *
		 * @return string
		 **/
		public function toJSON()
		{
			if ( $this->_deleted ) return $this->throwDeletedWarning(__METHOD__);

			return json::encode($this->_data);
		}
		
		/**
		 * Assign all field values at once from an associative array.
		 *
		 * @param array $array
		 * @return void
		 **/
		public function fromArray($array, $useHydrate=true)
		{
			if ( $this->_deleted ) return $this->throwDeletedWarning(__METHOD__);

			foreach ( $array as $key => $value )
				if ( isset($this->schema->fields[$key]) )
				{
					if ( $useHydrate ) $this->hydrate($key, $value);
					else $this->{$key} = $value;
				}
		}

		/**
		 * Checks whether the instance schema has a field named $field_name
		 *
		 * @param string $field_name
		 * @return boolean
		 **/
		public function hasField($field_name)
		{
			return array_key_exists($field_name, $this->schema->fields);
		}
		
		/**
		 * Prints out a detailed description of the model and it's current state.
		 *
		 * @param boolean $return
		 * @param boolean $recursive
		 * @return void|string
		 **/
		public function dump($recursive=true, $return=false, &$recursion_stack=array())
		{
			if ( $this->_deleted ) return $this->throwDeletedWarning(__METHOD__);
			
			// Simplified dump for Ajax calls
			if (request::isAjax())
			{
				// Calculate the padding
				$padLength = 0;
				foreach ( $this->schema->fields as $field )
				{
					if ( strlen($field->name) > $padLength )
					{
						$padLength = strlen($field->name);
					}
				}
				$padLength += 1; // Reserve one character for required asterisk

				// Header
				echo "\n[Model: {$this->model_name}]\n";
				
				// Dump
				foreach ( $this->schema->fields as $field )
				{
					echo str_pad($field->name . ($field->required ? '*' : ' '), $padLength)
						. ': '
						. $this->{$field->name} . "\n";
				}
				echo "\n";

				return;
			}

			if ( in_array($this, $recursion_stack, true) )
			{
				return XML::element('div', null, array(
					'title' => "{$this->model_name} {$this->_uid}",
					'class' => 'recursion stacked help'
					), 'recursion');
			}

			$table = XML::element('table', null, array( 'class' => 'kennel-debug'));

			// If not being called by Model or ModelSet, print the origin of the call
			$backtrace = debug_backtrace(0);
			if ( !isset($backtrace[1]['class']) || ( isset($backtrace[1]['class']) && !in_array($backtrace[1]['class'], array('Model', 'ModelSet')) ) )
			{
				$tr = XML::element('tr', $table);
				$td = XML::element('td', $tr, array('colspan'=>2));
				$td = XML::element('small', $td, null, $backtrace[0]['file'] . ' : ' . $backtrace[0]['line']);
			}

			// Add itself to the recursion stack
			$recursion_stack[] = $this;

			// Trigger the validation
			$this->validate();

			// Model name
			$tr = XML::element('tr', $table);
			$tth = XML::element('th', $tr, array('colspan' => '2'), "{$this->model_name} ");

			// Class name
			XML::element('code', $tth, null, get_class($this));

			// UID
			XML::element('span', $tth, array('class'=>'uid'), $this->_uid);

			// Sync status
			if ( $this->_edited )
				XML::element('span', $tth, array(
					'class' => 'edited-mark',
					'title' => 'edited'
					), 'e');

			// Fields
			foreach ( $this->schema->fields as $field )
			{
				// Name
				$tr = XML::element('tr', $table);
				$th = XML::element('th', $tr);
				XML::element('span', $th, array(
					'title' => $field->type . ($field->size ? " ({$field->size})" : ''),
					'class' => 'help'
					), $field->name);

				// Required
				if ( $field->required )
					XML::element('span', $th, array(
						'title' => 'required',
						'class' => 'required-mark'
						), '&#10033;');

				// Value
				$value_flags = 'value';
				if ( $field->type !== 'text')
				{
					$td = XML::element('td', $tr, null, (string) var_export($this->{$field->name}, true)   );
				}
				else
				{
					$td = XML::element('td', $tr);
					XML::element('pre', $td, array('class'=>'kennel-collapsible kennel-freeform'), nl2br(htmlentities($this->{$field->name})));
					XML::element('button', $td, array('class'=>'kennel-toggle-collapse', 'type'=>'button'), 'toggle');
				}

				// Edited value flag
				if ( $this->_data[$field->name] !== $this->_synced_data[$field->name] )
					$value_flags .= ' edited';

				// Invalid field mark and flag
				if ( array_key_exists($field->name, $this->invalid_fields) )
				{
					$value_flags .= ' invalid';
					XML::element('span', $td, array(
						'class' => 'invalid-mark',
						'title' => implode("\n", $this->invalid_fields[$field->name])
						), '!');
				}

				// Set the value field flags
				$td->class = $value_flags;
			}

			$polymorphic_classes = array();

			// Relationships
			foreach ( $this->schema->relationships as $rel )
			{
				// Polyphormic class for the title tag
				if ( $rel->as && !in_array($rel->as, $polymorphic_classes) )
					$polymorphic_classes[] = $rel->as;

				$tr = XML::element('tr', $table);

				// Relationship type
				$td = XML::element('th', $tr, array( 'colspan' => 2 ));
				switch ( $rel->type )
				{
					case Relationship::HAS_ONE:
						XML::element('span', $td, array(
							'class' => 'help', 'title' => 'has-one'
							), '&rarr;');
						break;
					case Relationship::BELONGS_TO:
						XML::element('span', $td, array(
							'class' => 'help', 'title' => 'belongs-to'
							), '&larr;');
						break;
					case Relationship::HAS_MANY:
						XML::element('span', $td, array(
							'class' => 'help', 'title' => 'has-many'
							), '&rArr;');
				}
				XML::text(' ', $td);

				// Relationship name
				XML::element('span', $td, array(
					'title' => $rel->destination . ( $rel->polymorphic && !$rel->as ? ' (polymorphic)' : '' ),
					'class' => 'help'
					), $rel->type === Relationship::BELONGS_TO && $rel->polymorphic ? " {$rel->name}" : $rel->name);

				// Stop here if not recursive
				if ( !$recursive ) continue;

				// Association dump
				$tr = XML::element('tr', $table);
				$nested = XML::element('td', $tr, array('class'=>'nested', 'colspan'=>2));

				if ( isset($this->_associations[$rel->name]) )
				{
					if ( $this->_associations[$rel->name]->isValid() )
						$nested->text = $this->{$rel->name}->dump(true, true, $recursion_stack);
					else
						XML::element('div', $nested, array('class'=>'null'), 'deleted');
				}
				else
				{
					// If there is no association made, then either:
					// a) It is loaded and still null, or this is a new model instance
					//    that has not yet been saved to the database.
					// b) It simply wasn't loaded yet
					if ( ( !$this->id || $this->id !== $this->_synced_data['id'] )
						|| ( isset($this->_assoc_status[$rel->name]) && $this->_assoc_status[$rel->name] ) )
						XML::element('div', $nested, array('class'=>'null'), 'null');
					else
						XML::element('div', $nested, array('class'=>'null'), 'not loaded');
				}

				foreach ( $polymorphic_classes as $polymorphic_classe)
					XML::element('span', $tth, array( 'class' => 'flag', ), $polymorphic_classe);
			}
			
			$style = html::css('kennel-debug.css');
			$script = html::js('kennel-debug.js');

			if ( $return ) return $style . $table . $script;
			else echo $style . $table . $script;
			return;
		}

		/**
		 * After a model instance is deleted from the database, it's internal
		 * deleted flag is set to true. If any instance method is called on that
		 * instance afterwards, we throw an error.
		 *
		 * @return void
		 * @author 
		 **/
		protected function throwDeletedWarning($method_name)
		{
			debug::warning("Model warning: called <code>$method_name</code> on the
				<code>{$this->model_name}</code> entry after deleting it's entry from
				the database.", null, 2);

			return false;
		}
		
		/**
		 * Instantiates and returns a new, empty $model_name instance. If a model
		 * class is found in the project, returns an instance of that class,
		 * otherwise uses the base Model class.
		 *
		 * @param string $model_name
		 * @return Model
		 **/
		static function instance($model_name)
		{
			$path = cascade::model($model_name);
			if ( !ORM::schema($model_name) )
				debug::error("Model error: schema for `{$model_name}` not found.");

			if ($path) $class = ucfirst($model_name) . '_model';
			else $class = 'Model';

			$instance = new $class($model_name);

			// Default values
			foreach ( ORM::schema($model_name)->fields as $field )
			{
				if ( $field->default !== null )
					$instance->{$field->name} = $field->default;
			}
			
			return $instance;
		}

		// TODO: associations ( and automatic their inverse associations ) are
		// spread all over the place and should be refactored into a one or a cople
		// of methods.
	}
?>