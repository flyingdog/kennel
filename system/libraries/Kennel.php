<?php
	require_once('system/helpers/cascade.php');
	require_once('system/helpers/request.php');
	require_once('system/helpers/debug.php');

	/**
	 * Returns a nicely formatted URL that conforms to the use_mode_rewrite
	 * setting. Local paths are a valid input, as well as complete URLs including
	 * scheme.
	 * 
	 * If no scheme is present, the current request scheme and domain name will
	 * be prepended to the passed argument, as strings representing domain names
	 * are valid path segments.
	 *
	 * @param string $uri
	 * @return string
	 **/
	function url($uri=null)
	{
		$scheme = (
			!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off'
			|| $_SERVER['SERVER_PORT'] == 443
		) ? 'https' : 'http';

		$parts = array_merge(
			parse_url($scheme . '://' . $_SERVER['HTTP_HOST']),
			parse_url($uri)
		);

		$root_url_path = substr(
			Kennel::$ROOT_PATH,
			strlen($_SERVER['DOCUMENT_ROOT'])
		);

		$path = '/' . trim($root_url_path, '/')
		. ( !Kennel::getSetting('application', 'use_mod_rewrite')
				? '/index.php' : null )
		. (
				isset($parts['path']) && !!str_replace('/', '', $parts['path']) ?
				'/' . trim($parts['path'], '/') . '/' : '/'
		);
		$path = preg_replace("/(\/)+/", "$1", $path);

		$url = $parts['scheme'] . '://'
			. (
					isset($parts['user']) ?
					$parts['user'] . (
						isset($parts['pass']) ?
						':' . $parts['pass'] : ''
					) . '@' : null
				)
			. $parts['host']
			. $path
			. ( isset($parts['query']) ? '?' . $parts['query'] : null )
			. ( isset($parts['fragment']) ? '#' . $parts['fragment'] : null );

			return $url;
	}

	class Kennel
	{
		public static $ROOT_PATH;
		public static $ROOT_URL;
		public static $MODULES = array();
		public static $REQUIRED_MODULES = array();
		public static $SETTINGS;
		public static $LOADED_BOOTSTRAPS = array();

		/**
		 * This method is the starting point for any Kennel application. It does
		 * three things:
		 * 
		 * 1. Set environment variables
		 * 2. Load the settings from settings.php
		 * 3. Load all modules
		 *
		 * @return void
		 **/
		static function init()
		{
			// Begin the benchmark
			$time_init = microtime(true);

			if ( array_key_exists('HTTP_HOST', $_SERVER) )
			{
			  // Running from a HTTP request

				// Set the root path
  			self::$ROOT_PATH = str_replace(
  				'\\', '/', dirname($_SERVER['SCRIPT_FILENAME'])
  			);
			  
			  $scheme = (
			  	!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off'
			  	|| $_SERVER['SERVER_PORT'] == 443
			  ) ? "https" : "http";
			  
			  // Set the root URL
  			self::$ROOT_URL = trim(
  				"{$scheme}://{$_SERVER['HTTP_HOST']}", '\\/' ) . '/'
  			  . trim( substr(self::$ROOT_PATH, strlen($_SERVER['DOCUMENT_ROOT'])),
  			  '\\/'
  			 );
			}
			else
			{
			  // Runing from the command line
  			self::$ROOT_PATH = str_replace('\\', '/', getcwd());
			}

			// Pass the initial time to the deub class
			debug::$TIME_INIT = $time_init;

			// Get the application settings
			if (file_exists('settings.php'))
			{
				require_once('settings.php');
				self::$SETTINGS = $settings;
			}
			else
			{
				Controller::call('GET', 'ksetup:firststeps');
			}

			// Fetch the modules
			self::fetch_modules();

			// Include the bootstrap files for the application and the framework
			$paths = array(
				self::$ROOT_PATH . '/system/bootstrap.php',
				self::$ROOT_PATH . '/application/bootstrap.php'
			);
			foreach ( $paths as $path )
			{
				if ( is_file($path) )
				{
					include($path);
					self::$LOADED_BOOTSTRAPS[] = $path;
				}
			}

			// Include the module bootstrap files
			foreach ( self::$MODULES as $module )
			{
				$path = self::$ROOT_PATH . "/modules/{$module}/bootstrap.php";
				if ( is_file($path) )
				{
					require_once($path);
					self::$LOADED_BOOTSTRAPS[] = $path;
				}
			}

			// Test for required modules
			$missing_modules = array();
			foreach ( self::$REQUIRED_MODULES as $module )
				if ( !in_array($module, self::$MODULES) )
					$missing_modules[] = $module;
			if ( count($missing_modules) )
				debug::error(
					'Kennel error: missing required modules - see Context.',
					$missing_modules
				);

			// Set the default timezone
			date_default_timezone_set(self::getSetting('application', 'timezone'));
		}

		/**
		 * Returns the value for $category > $setting from settings.php
		 *
		 * @param string $category
		 * @param string $setting
		 * @return mixed
		 **/
		public static function getSetting($category, $setting) {
			// Skip if no settings found
			if ( !self::$SETTINGS ) return null;
			if ( !array_key_exists($category, self::$SETTINGS) ||
				!array_key_exists($setting, self::$SETTINGS[$category] )
			)
			{
				debug::error("Setting [{$category}][{$setting}] was not found");
			}
			return self::$SETTINGS[$category][$setting];
		}

		private static function fetch_modules()
		{
			// Skip if the modules folder do not exist
			if ( !is_dir(self::$ROOT_PATH . '/modules') ) return null;
			
			// Get through the file list in the modules directory
			$files = array_diff(
				scandir(self::$ROOT_PATH . '/modules'),
				array('.', '..')
			);
			foreach ($files as $file)
			{
				if ( is_dir(self::$ROOT_PATH . '/modules/' . $file ) )
					self::$MODULES[] = $file;
			}
		}

		/**
		 * Register a $module as a requirement for the application to run. If that
		 * module is not present during initialization, Kennel will halt the
		 * execution at initialization with an error listing the missing required
		 * modules.
		 *
		 * @param string $module
		 * @return void
		 **/
		public static function requirement($module)
		{
			self::$REQUIRED_MODULES[] = $module;
		}

	}
?>