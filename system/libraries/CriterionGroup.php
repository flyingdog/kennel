<?php
	class CriterionGroup
	{
		var $logical;
		var $criteria = array();

		public function __construct($logical_operator=Criteria::L_NONE)
		{
			$this->logical = $logical_operator;
		}

		public function add($criterion)
		{
			// If the criterion is a plain string, bypass the logical operator verifications
			if (!is_string($criterion))
			{
				// The first criterion in the group must have no preceding logical operator
				if (count($this->criteria) == 0)
					$criterion->logical = Criteria::L_NONE;

				// For all but the first criterion in the group, the logical operator must not
				// be null, so we default to AND
				if (count($this->criteria) > 0 && $criterion->logical == Criteria::L_NONE)
					$criterion->logical = Criteria::L_AND;
			}

			$this->criteria[] = $criterion;
		}

		public function toString($criteria, $use_alias=true)
		{
			if ( empty($this->criteria) ) return null;

			$string = "";

			// Append the logical operator
			if ($this->logical)
				$string .= "\n{$this->logical} ";

			// Loop through and append the criteria
			$string .= '(';
			foreach ( $this->criteria as $n => $criterion )
			{
				if ( is_string($criterion) )
					$string .= "\n " . $criterion;
				else
					$string .= "\n " . $criterion->toString($criteria, $use_alias);
			}
			$string .= "\n)";
			return $string;
		}
		
	}
?>