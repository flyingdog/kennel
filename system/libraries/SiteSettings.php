<?php
	class SiteSettings
	{
		const BoolValue = 'bool';
		const StringValue = 'string';
		const IntValue = 'int';
		const FloatValue = 'float';

		private $settings;

		public function __construct()
		{
			$c = new Criteria('site_setting');
			$this->settings = $c->retrieve();
		}

		private function keyExists($key) {
			foreach ($this->settings as $setting) {
				if ($setting->key === $key) return true;
			}
			return false;
		}

		private function getSetting($key, $type) {
			$setting = null;

			foreach ($this->settings as $existing) {
				if ($existing->key === $key) $setting = $existing;
			}

			if ($setting === null) {
				$setting = Model::instance('site_setting');
				$setting->key = $key;
				$setting->type = $type;
			}

			return $setting;
		}

		// Get functions

		public function get($key, $value, $type) {
			switch ($type) {
				case self::BoolValue: $this->getBool($key); break;
				case self::StringValue: $this->getString($key); break;
				case self::IntValue: $this->getInt($key); break;
				case self::FloatValue: $this->getFloat($key); break;
			}
		}

		public function getBool($key, $default=false)
		{
			if (!is_bool($default)) {
				debug::error("SiteSettings::getBool error: default value for `{$key}` must be boolean.");
			}

			$value = $this->keyExists($key) ? $this->getSetting($key, 'bool')->value : $default;
			
			if ($value === null) return $default;
			elseif ($value === 'true') return true;
			elseif ($value === 'false') return false;
			else return $default;
		}

		public function getString($key, $default="") {
			if (!is_string($default)) {
				debug::error("SiteSettings::getString error: default value for `{$key}` must be a string.");
			}

			$value = $this->keyExists($key) ? $this->getSetting($key, 'string')->value : $default;
			return strval($value);
		}

		public function getInt($key, $default=0) {
			if (!is_numeric($default)) {
				debug::error("SiteSettings::getInt error: default value for `{$key}` must be an integer.");
			}

			$value = $this->keyExists($key) ? $this->getSetting($key, 'int')->value : $default;
			if (!is_numeric($value)) {
				debug::error("SiteSettings::getInt error: `{$key}` value of `{$value}` cannot be parsed as an integer.");
			}
			return intval($value);
		}

		public function getFloat($key, $default=0.0) {
			if (!is_numeric($default)) {
				debug::error("SiteSettings::getBool error: default value for `{$key}` must be numeric.");
			}

			$value = $this->keyExists($key) ? $this->getSetting($key, 'float')->value : $default;
			if (!is_numeric($value)) debug::error("SiteSettings::getFloat error: `{$key}` value of `{$value}` cannot be parsed as a float.");
			return floatval($value);
		}

		// Set functions

		public function set($key, $value, $type) {
			switch ($type) {
				case self::BoolValue: $this->setBool($key, $value); break;
				case self::StringValue: $this->setString($key, $value); break;
				case self::IntValue: $this->setInt($key, $value); break;
				case self::FloatValue: $this->setFloat($key, $value); break;
			}
		}

		public function setBool($key, $value) {
			// Validate
			if (!is_bool($value)) {
				debug::error("SiteSettings::setBool error: `{$value}` for key `{$key}` must be a boolean.");
			}

			$setting = $this->getSetting($key, self::BoolValue);
			$setting->value = $value ? 'true' : 'false';
			$setting->save();
		}

		public function setString($key, $value) {
			$setting = $this->getSetting($key, self::StringValue);
			$setting->value = strval($value);
			$setting->save();
		}

		public function setInt($key, $value) {
			if (!is_numeric($value)) {
				debug::error("SiteSettings::setInt error: `{$value}` for key `{$key}` must be an integer.");
			}
			$value = intval($value);

			$setting = $this->getSetting($key, self::IntValue);
			$setting->key = $key;
			$setting->type = 'int';
			$setting->value = intval($value);
			$setting->save();
		}

		public function setFloat($key, $value) {
			if (!is_numeric($value)) {
				debug::error("SiteSettings::setFloat error: `{$value}` for key `{$key}` must be numeric.");
			}
			$value = floatval($value);

			$setting = $this->getSetting($key, self::BoolValue);
			$setting->value = floatval($value);
			$setting->save();
		}

		// Debug utilities

		public function dump() {
			foreach ($this->settings as $setting) {
				$setting->dump();
			}
		}

	}
?>