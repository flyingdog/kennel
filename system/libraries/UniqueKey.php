<?php
	class UniqueKey {
		var $name;
		var $fields = array();

		function __construct(DOMElement $element)
		{
			$this->name = $element->getAttribute('name');
			$this->fields = array();

			$rawFields = $element->getAttribute('fields');
			$fields = explode(',', $rawFields);

			foreach ( $fields as $field )
			{
				$this->fields[] = trim($field);
			}
		}

		public function createString()
		{
			$quotedFields = array();
			foreach ( $this->fields as $fieldName )
			{
				$quotedFields[] = "`{$fieldName}`";
			}
			return "UNIQUE KEY `{$this->name}` (" . implode(', ', $quotedFields) . ')';
		}
	}
?>