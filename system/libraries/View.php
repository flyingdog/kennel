<?php
	/**
	 * Small utility function that prints CSS classes based on a numeric index:
	 * first, last, odd, even
	 *
	 * @return void
	 * @author 
	 **/
	function c($n, $max=null)
	{
		if ( is_array($max) ) $max = count($max);
		
		$classes = array();
		if ( $n === 0 ) $classes[] = 'first';
		if ( $max !== null && $n + 1 === $max ) $classes[] = 'last';
		$classes[] = "n-{$n}";
		$classes[] = $n % 2 ? 'odd' : 'even';
		echo implode(' ', $classes);
	}

	/**
	 * Small utility function to be used in iterations that prints a $comma
	 * if the zero-based $index is not yet at the end of $count. $count can be
	 * either an int or an array
	 *
	 * @return void
	 * @author 
	 **/
	function comma($index, $count, $comma=', ')
	{
		if ( !is_int($count) )
			$count = count($count);

		if ( $index + 1 < $count )
			echo $comma;
	}

	class View
	{
		private $view;
		private $parent_view;
		private $vars = array();
		
		function __construct($view, $parentView=null)
		{
			$this->view = $view;
			if ($parentView)
				$this->parent_view = $parentView;
		}
		
		function __toString()
		{
			return strval($this->output());
		}
		
		function __get($var)
		{
			if(isset($this->vars[$var])) return $this->vars[$var];
			else debug::error("'{$var}' is not defined");
		}
		
		function __set($var, $value)
		{
			$this->set($var, $value);
		}

		function set($var, $value) {
			if (is_object($value) && get_class($value) == 'View')
				$value->parent_view = $this;
				
			$this->vars[$var] = $value;
		}
		
		function getTemplateVars()
		{
			return $this->vars;
		}
		
		function output() {
			// Set all template variables
			foreach ($this->vars as $var =>$val)
				$$var = $val;
			if($this->parent_view)
				foreach ($this->parent_view->vars as $var =>$val)
					$$var = $val; // TODO: Check extract() for an alternative method
			
			$path = cascade::view($this->view);
			if (!$path) return debug::error("View <strong>{$this->view}</strong> not found.");
			
			//begin intercepting the output buffer
			ob_start();
			
			if($path) require($path);
			
			//unset all template variables
			foreach ($this->vars as $var =>$val) {
				unset($$var);
			}
			
			//return the output and close the buffer
			$output = ob_get_clean();
			return $output;
		}
		
		function render()
		{
			print $this->output();
		}
	}
?>
