<?php
/*               __
                / _)
       _/\/\/\_/ /
     _|         /
   _|  (  | (  |      OMG A DINOSAUR
  /__.-'|_|--|_|

*/
	class ORM
	{
		static $DB;
		private static $_SCHEMA_CACHE = array();
		
		function __construct()
		{
			self::$DB = new MySQL;
		}
		
		static function retrieve(Criteria $criteria)
		{
			if (!self::$DB) self::$DB = new MySQL;
			
			$schema = self::schema($criteria->from_model_name);
			
			// Add the necessary joins to eager load has-one associations
			self::addAssociationJoins($criteria);
			
			$sql = self::selectString($criteria);
			$rs = self::$DB->query($sql);
			
			$model_array = array();
			while ( $data = self::$DB->fetch($rs) )
			{
				// Instantiate the main criteria model and hydrate it with the
				// database results
				$model = Model::instance($criteria->from_model_name);
				self::hydrateModel($model, $model->model_name, $data);
				
				// HAS_ONE relationships are eager-loaded, and we can instantiate and
				// hydrate them too.
				foreach ( $schema->relationships as $rel )
				{
					if ( $rel->type === Relationship::HAS_ONE )
					{
						// If not present in the returned data, this association was
						// excluded from the retrieval.
						// If the returned value is null, this this association is null.
						if ( isset($data->{"{$rel->name}__id"}) && $data->{"{$rel->name}__id"} )
						{
							// Instantiate the child model and hydrate it with the database
							// results
							$child = Model::instance($rel->destination);
							self::hydrateModel($child, $rel->name, $data);

							// Association of parent->child
							$model->{$rel->name} = $child;

							// Inverse association (child->parent) - TODO: do we really need
							// to do this?
							
							// if ( $rel->polymorphic )
							// {
							// 	$child->assoc($rel->as, $model, $rel->name);
							// }
							// else
							// {
							// 	// We find the inverse relationship by looking for a
							// 	// relationship with the destination pointing to our $model
							// 	// name.
							// 	foreach ( $model->{$rel->name}->schema->relationships as $name => $inverse )
							// 		if ( $inverse->type === Relationship::BELONGS_TO
							// 			&& $inverse->destination === $model->model_name )
							// 			$child->{$inverse->name} = $model;
							// }

							// hydrate
						}
					}
					// else if ( $rel->type === Relationship::BELONGS_TO )
					// {
					// 	// If not present in the returned data, this association was
					// 	// excluded from the retrieval.
					// 	// If the returned value is null, this this association is null.
					// 	if ( isset($data->{"{$rel->name}__id"}) && $data->{"{$rel->name}__id"} )
					// 	{
					// 		// Only non-polymorphic allowed
					// 		if ( !$rel->polymorphic )
					// 		{
					// 			// Instantiate and hydrate the child with the database results
					// 			$child = Model::instance($rel->destination);
					// 			self::hydrateModel($child, $rel->name, $data);

					// 			// Association of child->parent
					// 			$model->{$rel->name} = $child;
								
					// 			// Inverse association (parent->child)

					// 			// We find the inverse relationship by looking for a
					// 			// relationship with the destination pointing to our $model
					// 			// name.
					// 			foreach ( $model->{$rel->name}->schema->relationships as $name => $inverse )
					// 				if ( $inverse->type === Relationship::BELONGS_TO
					// 					&& $inverse->destination === $model->model_name )
					// 					$model->{$rel->name}->{$inverse->name} = $model;
					// 		}

					// 		// hydrate
					// 	}

					// } // end rel-type if
				}
				
				$model_array[] = $model;
			}
			
			return $model_array;
		}
		
		static function count(Criteria $criteria)
		{
			if (!self::$DB) self::$DB = new MySQL;
			
			self::addAssociationJoins($criteria);
			
			$sql = self::selectString($criteria);
			$rs = self::$DB->query($sql);
			return self::$DB->numRows($rs);
		}
		
		static function dump(Criteria $criteria, $return=false)
		{
			if (!self::$DB) self::$DB = new MySQL;
			
			self::addAssociationJoins($criteria);
			
			$sql = self::selectString($criteria);

			if ( Kennel::$ROOT_URL )
				$sql = syntax::mysql($sql); // Running from HTTP request
			
			if ( $return ) return debug::dump($sql, true);
			else debug::dump($sql);
		}

		static function addAssociationJoins(Criteria $criteria)
		{
			$schema = self::schema($criteria->from_model_name);

			foreach ($schema->relationships as $rel)
			{
				if ( $rel->type === Relationship::HAS_ONE )
				{
					// If the alias is in the exclusion list, don't add the join
					// TODO: we should use the inverse approach, have an inclusions
					// list and join with those only.
					if ( in_array($rel->name, $criteria->exclusions) )
						continue;

					if ( $rel->polymorphic )
					{
						// Adding to the exclusion list is done via $c->exclude($alias);

						$on = array(
							"{$rel->name}.{$rel->as}_id" => "{$criteria->from_model_name}.id",
							"{$rel->name}.{$rel->as}_model" => $rel->origin,
							"{$rel->name}.{$rel->as}_rel" => $rel->name
							);
						$join = $criteria->addJoin(array(
							'model_name' => $rel->destination,
							'alias' => $rel->name
							), $on, Criteria::LEFT_JOIN);
					}
					else
					{
						$on = array(
							"{$rel->destination}.{$rel->origin}_id" => "{$criteria->from_model_name}.{$schema->primary_key()->name}"
							);
						$join = $criteria->addJoin($rel->name, $on, Criteria::LEFT_JOIN);
					}
				}
				else if ( $rel->type === Relationship::BELONGS_TO )
				{
					// If the alias is in the exclusion list, don't add the join
					// TODO: we should use the inverse approach, have an inclusions
					// list and join with those only.

					if ( in_array($rel->name, $criteria->exclusions) )
						continue;

					// We can only eager-load the non-polymorphic associations
					if ( !$rel->polymorphic )
					{
						$on = array(
							"{$rel->name}.id" => "{$criteria->from_model_name}.{$rel->name}_id"
							);
						$join = $criteria->addJoin(array('model_name'=>$rel->destination, 'alias' => $rel->name), $on, Criteria::LEFT_JOIN);
					}

				}
			}
		}
		
		static function retrieveFirst(Criteria $criteria)
		{
			$criteria->limit(1);
			$items = self::retrieve($criteria);
			if (count($items) > 0) return $items[0];
			else return null;
		}
		
		static function delete(Criteria $criteria)
		{
			if (!self::$DB) self::$DB = new MySQL;
			$sql = self::deleteString($criteria);
			self::$DB->query($sql);
			return self::$DB->affected();
		}
		
		static function create($model)
		{
			if (!self::$DB) self::$DB = new MySQL;
			
			$schema = self::schema($model);
			$sql = $schema->createString();
			
			self::$DB->query($sql);
			return self::$DB->affected();
		}
		
		static function retrieveByPrimaryKey($model_name, $primary_key_value)
		{
			$schema = self::schema($model_name);
			$primaryKey = $schema->primary_key();
			if(!$primaryKey) debug::error("ORM::retrieveByPrimaryKey error: no primary key defined for model <strong>{$model_name}</strong>.");
			
			$c = new Criteria($model_name);
			$c->add($primaryKey->name, $primary_key_value);
			$c->limit(1);
			
			$instancies = ORM::retrieve($c);
			
			if (isset($instancies[0])) return $instancies[0];
			else return null;
		}
		
		static function retrieveAll($model_name)
		{
			$c = new Criteria($model_name);
			return self::retrieve($c);
		}
		
		static function schema($model_name)
		{
			if ( !isset(self::$_SCHEMA_CACHE[$model_name]) )
				self::$_SCHEMA_CACHE[$model_name] = new Schema($model_name);
			
			return self::$_SCHEMA_CACHE[$model_name];
		}
		
		/**
		 * Parses column name from the database $data as {$alias}__{field}, then
		 * assigns that column value to the $model->field via $model->hydrate(),
		 * which won't flag the model instance as edited and won't propagate the
		 * primary key.
		 * 
		 * @param Model $model
		 * @param string $alias
		 * @param array $data
		 **/
		static function hydrateModel($model, $alias, $data)
		{
			foreach ( $data as $key => $value )
			{
				// Reference begins with the table name...
				if ( substr($key, 0, strlen($alias)) == $alias
					// ...imediatelly followed by double underscores
					&& substr($key, strlen($alias), 2) === '__'
					)
				{
					$field_name = substr($key, strlen($alias . '__'));
					$model->hydrate($field_name, $value);
				}
			}
		}
		
		// ORM::selectString(Criteria $criteria)
		// If used to populate models, $use_alias must be true
		static function selectString(Criteria $criteria, $use_alias=true)
		{
			// SELECT
			$sql = "SELECT ";
			$sql .= self::fieldListString($criteria, $use_alias);
			
			// FROM
			$sql .= "\nFROM ";
			$sql .= self::fromString($criteria);
			
			// JOINS
			$join_string = self::joinString($criteria);
			if ($join_string) $sql .= $join_string;
			
			// WHERE
			$where_string = self::whereString($criteria);
			if ($where_string) $sql .= "\nWHERE {$where_string}";
			
			// GROUP
			$group_string = self::groupString($criteria);
			if ($group_string) $sql .= "\nGROUP BY {$group_string}";
			
			// ORDER
			$order_string = self::orderString($criteria);
			if ($order_string) $sql .= "\nORDER BY {$order_string}";
			
			// LIMIT
			$limit_string = self::limitString($criteria);
			if($limit_string) $sql .= "\nLIMIT {$limit_string}";
			
			$sql .= ';';
			
			return $sql;
		}
		
		static function deleteString(Criteria $criteria)
		{
			// DELETE
			$sql = "DELETE ";
			
			// FROM
			$sql .= "\nFROM ";
			$sql .= self::fromString($criteria, false);
			
			// WHERE
			$where_string = self::whereString($criteria, false);
			if ($where_string) $sql .= "\nWHERE {$where_string}";
			
			$sql .= ';';
			
			return $sql;
		}

		
		static function fieldListString(Criteria $criteria, $use_alias=true)
		{
			$select_array = array();
			
			$schema = self::schema($criteria->from_model_name);

			foreach ( $schema->fields as $field )
			{
				$select_string = "\n `{$criteria->from_model_name}`.`{$field->name}`";

				if ( $use_alias )
					$select_string .= " AS `{$criteria->from_model_name}__{$field->name}`";

				$select_array[] = $select_string;
			}
			
			foreach ( $criteria->joins as $join )
			{
				$schema = self::schema($join['model_name']);

				foreach ( $schema->fields as $field )
				{
					$select_string = "\n `{$join['alias']}`.`{$field->name}`";

					if ( $use_alias )
					{
						if ( isset($join['alias']) )
							$select_string .= " AS `{$join['alias']}__{$field->name}`";
						else
							$select_string .= " AS `{$join['alias']}__{$field->name}`";
					}

					$select_array[] = $select_string;
				}
			}
			
			$select_string = implode(', ', $select_array);
			
			return $select_string;
		}
		
		static function fromString(Criteria $criteria , $use_alias=true)
		{
			$schema = self::schema($criteria->from_model_name);

			$from_string = "\n `{$schema->table}`";

			if ( $use_alias )
				$from_string .= " as `$criteria->from_model_name`";

			return $from_string;
		}
		
		static function joinString(Criteria $criteria)
		{
			$joins = array();
			foreach( $criteria->joins as $join )
			{
				$schema = self::schema($join['model_name']);

				$on = array();
				foreach ( $join['on'] as $left => $right )
				{
					$left = MySQL::escape($left);
					$right = MySQL::escape($right);
					$on[] = ( strstr($left, '.') ? self::formatColumnReference($left, $criteria) : '"'.$left.'"' )
						. ' = ' . (strstr($right, '.' ) ? self::formatColumnReference($right, $criteria) : '"'.$right.'"');
				}
				$on_string = implode(' AND ', $on);

				$table_name = "`{$schema->table}` as `{$join['alias']}`"  ;

				// See Criteria::addJoin() for more information on joins with criteria.
				if ( array_key_exists('pre_select_criteria', $join) )
					$from = '('
						. rtrim(self::selectString($join['pre_select_criteria'], false), ';')
						. ") {$table_name}";
				else
					$from = $table_name;

				$joins[] = "\n {$join['join_type']} {$from} ON {$on_string}";

			}
			
			return implode('', $joins);
		}
		
		static function whereString(Criteria $criteria, $use_alias=true)
		{
			$whereString = "";
			foreach ($criteria->criterion_groups as $group)
			{
				$whereString .= $group->toString($criteria, $use_alias);
			}
			return $whereString;
		}
		
		static function orderString(Criteria $criteria)
		{
			$order_params = array();
			
			foreach($criteria->order_by as $order_by)
				if ($order_by['direction'] == 'ASC' or $order_by['direction'] == 'DESC')
					$order_params[] = "\n " . self::formatColumnReference($order_by['column'], $criteria) . " {$order_by['direction']}";
				elseif ($order_by['direction'] == 'RAND')
					$order_params[] = "\n " . "RAND()";
			
			return implode(', ', $order_params);
		}
		
		static function groupString(Criteria $criteria)
		{
			$params = array();
			foreach($criteria->group_by as $group_by)
				$params[] = "\n " . self::formatColumnReference($group_by, $criteria);
			
			return implode(', ', $params);
		}
		
		static function limitString(Criteria $criteria)
		{
			if ($criteria->limit)
			{
				if ($criteria->offset) return "{$criteria->offset}, {$criteria->limit}";
				else return "{$criteria->limit}";
			}
			else
				return null;
		}
		
		/**
		 * When joins are used, queries require columns to be referenced as
		 * {table}.{column}. This method encloses each table and column names in
		 * backticks to avoid conflicting with SQL keywords.
		 *
		 * Since the Criteria that the query is being performed on is passed too,
		 * we can also allow the user to omit the model name when refering to the
		 * base model being queried. This behavior can be disabled by passing false
		 * to the third parameter, in which case $reference_strings without dots
		 * are treated as values.
		 *
		 * @param string $reference_string
		 * @param Criteria $criteria
		 * @param boolean $require_dot_notation
		 * @return string
		 **/
		static function formatColumnReference($reference_string, Criteria $criteria, $prepend_alias=true)
		{
			// Reference has table/column
			if ( strstr($reference_string, '.') )
			{
				$table = trim(strtok($reference_string, '.'), '`');
				$column = trim(strtok('.'), '`');
				return "`{$table}`.`{$column}`";
			}

			// Just the column name, prepend the alias
			else if ( $prepend_alias )
			{
				$schema = ORM::schema($criteria->from_model_name);
				return "`{$schema->model_name}`.`{$reference_string}`";
			}

			// Just add the backticks
			else
			{
				return "`{$reference_string}`";
			}
		}

	}
	
?>
