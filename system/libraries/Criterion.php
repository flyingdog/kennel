<?php

	class Criterion
	{
		var $column;
		var $value;
		var $operator;
		var $logical;
		
		function __construct($column, $value, $operator, $logical)
		{
			$this->column = $column;
			$this->value = $value;
			$this->operator = $operator;
			$this->logical = $logical;
		}

		function toString($criteria, $use_alias=true)
		{
			$string = "";

			// Preceding logical operator
			if ( $this->logical )
				$string .= "$this->logical ";

			if ( $this->operator == Criteria::MATCH ) {
				// Match; match criterions support arrays as the column reference
				if (is_array($this->column)) {
					$formatted_references = array();
					foreach ($this->column as $column_reference) {
						$formatted_references[] = ORM::formatColumnReference($column_reference, $criteria, $use_alias);
					}
					$match_string = implode(', ', $formatted_references);
				}
				else $match_string = ORM::formatColumnReference($this->column, $criteria, $use_alias);
				
				// Against
				$terms = explode(' ', $this->value);
				foreach ($terms as $key => $term) {
					$term = MySQL::escape($term);
					$terms[$key] = "+${term}";
				}
				$query_string = implode(' ', $terms);

				$string .= "MATCH(${match_string}) AGAINST('${query_string}' IN NATURAL LANGUAGE MODE)";
			} 
			else
			{
				$formattedColumnReference = ORM::formatColumnReference($this->column, $criteria, $use_alias);

				// Criteria::NOW
				if ($this->value === Criteria::NOW)
					$string .= $formattedColumnReference . ' ' . $this->operator . ' NOW()';

				// NULL value or Criteria::IS_NULL
				elseif ($this->value === NULL || $this->value === Criteria::IS_NULL)
					$string .= $formattedColumnReference . ' IS NULL';

				// Criteria::IS_NOT_NULL
				elseif ($this->value === Criteria::IS_NOT_NULL)
					$string .= $formattedColumnReference . ' IS NOT NULL';

				// IN operator
				elseif (($this->operator == Criteria::IN || $this->operator == Criteria::NOT_IN) && is_array($this->value))
					$string .= $formattedColumnReference . " {$this->operator} (" . $this->formatValue($this->value) . ')';

				// X = Y
				else
					$string .= $formattedColumnReference . ' ' . $this->operator . ' ' . $this->formatValue($this->value);
			}

			return $string;
		}

		private function formatValue($value)
		{
			$value = MySQL::escape($value);
			if (is_array($value)) {
				foreach ($value as $i=>$v) {
					$value[$i] = is_string($v) ? '"'.$v.'"' : $v;
				}
				return implode(', ', $value);
			}
			else {
				return is_string($value) ? '"' . $value . '"' : $value;
			}
		}

	}
	
?>