<?php
	class Relationship
	{
		const HAS_ONE = 'has-one';
		const BELONGS_TO = 'belongs-to';
		const HAS_MANY = 'has-many';

		var $name;

		var $origin;
		var $type = self::HAS_ONE;
		var $destination;

		var $as;
		var $polymorphic = false;

		// var $foreign_key;

		/**
		 * The constructor takes the name of the model this relationship was
		 * declared from as $origin, and the DOM $element representing it from the
		 * Schema XML, which is then parsed to set the instance properties.
		 *
		 * @param string $origin
		 * @param DOMElement $element
		 * @return void
		 **/
		function __construct($origin, DOMElement $element)
		{
			$this->origin = $origin;

			// has-one association
			if ( $element->hasAttribute(self::HAS_ONE) )
			{
				$this->type = self::HAS_ONE;

				self::rejectAttributes($element, $this->type, array(
					'belongs-to', 'has-many', 'polymorphic'
				));

				$this->destination = $element->getAttribute(self::HAS_ONE);

				if ( $element->hasAttribute('as') )
				{
					$this->polymorphic = true;
					$this->as = $element->getAttribute('as');
				}
			}

			// belongs-to association
			elseif ( $element->hasAttribute(self::BELONGS_TO) )
			{
				$this->type = self::BELONGS_TO;

				self::rejectAttributes($element, $this->type, array(
					'has-one', 'has-many', 'as'
				));

				$this->destination = $element->getAttribute(self::BELONGS_TO);

				if ( $element->hasAttribute('polymorphic')
				&& $element->getAttribute('polymorphic') === 'true' )
				{
					$this->polymorphic = true;
				}
			}

			// has-many association
			elseif ( $element->hasAttribute(self::HAS_MANY) )
			{
				$this->type = self::HAS_MANY;

				self::rejectAttributes($element, $this->type, array(
					'has-one', 'belongs-to', 'polymorphic'
				));

				$this->destination = $element->getAttribute(self::HAS_MANY);

				if ( $element->hasAttribute('as') )
				{
					$this->polymorphic = true;
					$this->as = $element->getAttribute('as');
				}
			}

			else {
				debug::error("Relationship error: the relationship type was not declared
					in the `{$origin}` model");
			}

			// Name defaults to the destionation if omitted
			if ( $element->hasAttribute('name') )
				$this->name = $element->getAttribute('name');
			else
				$this->name = $this->destination;
		}

		/**
		 * Returns a Field instance corresponding to the foreign key to be injected
		 * into the Schema. If this is a polymorphic association, also returns the
		 * fields corresponding to the foreign model and relationship.
		 *
		 * @param string $table_name
		 * @return array $array
		 **/
		public function foreignKeyAndTypeFields($table_name)
		{
			$fields = array();
			if ( $this->type === self::BELONGS_TO )
			{
				$field = new Field($table_name);
				$field->name = "{$this->name}_id";
				$field->type = 'int';
				$field->size = 12;
				$fields[] = $field;

				if ( $this->polymorphic )
				{
					$field = new Field($table_name);
					$field->name = "{$this->name}_model";
					$field->type = 'char';
					$field->size = '128';
					$fields[] = $field;

					$field = new Field($table_name);
					$field->name = "{$this->name}_rel";
					$field->type = 'char';
					$field->size = '128';
					$fields[] = $field;
				}
			}
			return $fields;
		}

		/**
		 * Some attributes in the Schema XML declaration for the relationship
		 * are mutually exclusive. This method ensures the validity of the
		 * $element object by receiving a list of argument names to reject.
		 *
		 * @param DOMElement $element
		 * @param $attributes
		 **/
		private static function rejectAttributes(DOMElement $element, $relationship_type, $attributes)
		{
			foreach ( $attributes as $attr )
			{
				if ( $element->hasAttribute($attr) )
					debug::error("Relationship error: `{$attr}` cannot be declared in a
						`{$relationship_type}` relationship", $element, 2);
			}
		}

	} // END class
?>