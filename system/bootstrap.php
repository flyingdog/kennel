<?php
	router::get( '/notfound',            'main:notfound'       );
	router::get( '/ksetup',              'ksetup:index'        );
	router::get( '/ksetup/createmodels', 'ksetup:createmodels' );
	router::get( '/ksetup/database',     'ksetup:database'     );
	router::get( '/ksetup/admin-user',   'ksetup:admin-user'   );
	router::get( '/ksetup/create-admin',  'ksetup:create-admin');
	router::get( '/ksetup/firststeps',   'ksetup:firststeps'   );
	router::get( '/ksetup/migrations',   'ksetup:migrations'   );
	router::get( '/ksetup/modules',      'ksetup:modules'      );
	router::get( '/ksetup/settings',     'ksetup:settings'     );
	router::get( '/ksetup/startpage',    'ksetup:startpage'    );
	router::post('/ksetup/login',        'ksetup:login'        );


	/**
	 * Shortcut for `new Criteria($model_name)`. Instantializes and returns a
	 * Criteria object.
	 *
	 * @param string $model_name
	 * @return Criteria
	 **/
	function Crit($model_name) {
		return new Criteria($model_name);
	}
?>