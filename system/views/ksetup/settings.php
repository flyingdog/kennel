<h1>Settings</h1>

<table>
	<?php foreach ( $settings as $section=>$section_settings ): ?>
		<tr>
			<th colspan="2"><?php echo $section; ?></th>
		</tr>
		<?php foreach ($section_settings as $key=>$val): ?>
		<tr>
			<td><?php echo $key ?></td>
			<td>
				<?php
					if ( is_bool($val) ) echo $val?'yes':'no';
					elseif ( in_array($key, array('user', 'pass')) ) echo str_repeat('*', 16);
					else echo $val;
				?>
			</td>
		</tr>
		<?php endforeach ?>
	<?php endforeach; ?>
</table>