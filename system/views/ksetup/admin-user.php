<h1>Admin user</h1>

<?php if (flash::has('created')): ?>
	<p class="msg ok"><?php echo flash::get('created'); ?></p>
<?php endif; ?>

<?php if (flash::has('error')): ?>
	<p class="msg error"><?php echo flash::get('error'); ?></p>
<?php endif; ?>

<?php if ($firstUser): ?>
	<br>
	<p>
		Database already contain registered users.
	</p>
<?php else: ?>
	<br>
	<p>No users found in the database.</p>
	<br>
	<p>
		<a href="<?php print url('ksetup/create-admin'); ?>" class="action">Create Admin User</a>
	</p>
<?php endif; ?>