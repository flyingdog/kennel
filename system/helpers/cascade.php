<?php
	function __autoload($class_name)
	{
		// Controllers
		if (substr($class_name, -11) == '_controller')
		{
			// $controller_name = strtolower(substr($class_name, 0, (strlen($class_name) - 11)));
			$reference = Controller::reference($class_name);
			$path = cascade::controller($reference);
			if ($path) return require_once($path);
			else debug::error(
				"<strong>__autoload:</strong> class <code>{$class_name}</code> not found",
				array('reference'=>$reference, 'path'=>$path)
				);
		}

		// Helpers
		if (ctype_lower($class_name[0]))
		{
			$path = cascade::helper($class_name);
			if($path) return require_once($path);
		}

		// Models
		if (substr($class_name, -6) == '_model')
		{
			$model_name = strtolower(substr($class_name, 0, (strlen($class_name) - 6)));

			$path = cascade::model($model_name);
			if($path) return require_once($path);
		}

		// Libraries (allow use of namespaces, files must be organized in folders with the same structure)
		$path = cascade::library(str_replace('\\', '/', $class_name));
		if($path) return require_once($path);

		// Nothing!
		debug::error("<strong>__autoload:</strong> class <code>'$class_name'</code> not found.");
	}

	/**
	 * The cascade helper is the class responsible for the cascading feature of
	 * the framework.
	 **/
	class cascade
	{
		private static $_CACHE;

		/**
		 * Returns the location of the library specified in $resource, or null if
		 * not found.
		 *
		 * @param string $resource
		 * @return string
		 **/
		static function library($resource)
		{
			return self::locate("/libraries/{$resource}.php");
		}

		/**
		 * Returns the location of the schema specified in $resource, or null if
		 * not found.
		 *
		 * @param string $resource
		 * @return string
		 **/
		static function schema($resource)
		{
			return self::locate("/models/{$resource}.xml");
		}

		/**
		 * Returns the location of the model specified in $resource, or null if
		 * not found.
		 *
		 * @param string $resource
		 * @return string
		 **/
		static function model($resource)
		{
			return self::locate("/models/{$resource}.php");
		}

		static function view($resource)
		{
			return self::locate("/views/{$resource}.php");
		}

		static function controller($resource)
		{
			return self::locate("/controllers/{$resource}.php");
		}

		static function helper($resource)
		{
			return self::locate("/helpers/{$resource}.php");
		}

		static function css($resource)
		{
			return self::locate("/assets/css/{$resource}", true);
		}

		static function img($resource)
		{
			return self::locate("/assets/img/{$resource}", true);
		}

		static function js($resource)
		{
			return self::locate("/assets/js/{$resource}", true);
		}

		static function file($resource)
		{
			return self::locate("/assets/files/{$resource}", true);
		}

		static function video($resource)
		{
			return self::locate("/assets/videos/{$resource}", true);
		}

		public static function locate($path, $return_url=false)
		{
			if (isset(self::$_CACHE[$path]))
			{
				if ($return_url) return Kennel::$ROOT_URL . self::$_CACHE[$path];
				else return Kennel::$ROOT_PATH . self::$_CACHE[$path];
			}

			// Application (user) resource
			if (is_file(Kennel::$ROOT_PATH . '/application' . $path))
			{
				self::$_CACHE[$path] = '/application' . $path;
				if ($return_url) return Kennel::$ROOT_URL . '/application' . $path;
				else return Kennel::$ROOT_PATH . '/application' . $path;
			}

			// Module resource
			foreach (Kennel::$MODULES as $module)
			{
				if (is_file(Kennel::$ROOT_PATH . '/modules' . '/' . $module . $path))
				{
					self::$_CACHE[$path] = '/modules' . '/' . $module . $path;
					if ($return_url) return Kennel::$ROOT_URL . '/modules' . '/' . $module . $path;
					else return Kennel::$ROOT_PATH . '/modules' . '/' . $module . $path;
				}
			}

			// System resource
			if(is_file(Kennel::$ROOT_PATH . "/system" . $path))
			{
				self::$_CACHE[$path] = '/system' . $path;
 				if ($return_url) return Kennel::$ROOT_URL . '/system' . $path;
				else return Kennel::$ROOT_PATH . '/system' . $path;
			}
		}
	}
?>
