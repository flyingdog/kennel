<?php
	// Array helpers
	class a
	{
		public static function occurrences($needle, $haystack)
		{
			$counter = 0;
			foreach ( $haystack as $hay )
			{
				if ( $hay === $needle )
					$counter ++;
			}
			return $counter;
		}

		public static function flatten($array, $return=null)
		{
			if (!$return) $return = array();
			
			for ($x = 0; $x < count($array); $x++)
				if (is_array($array[$x]))
				  $return = self::flatten($array[$x],$return);
				elseif ($array[$x]) 
					$return[] = $array[$x];
			return $return;
		}
		
		public static function split($array, $size=5)
		{
			$result = array();
			while (count($array) >= $size)
			{
				$result[] = array_slice($array, 0, $size);
				$array = array_slice($array, $size);
			}
			
			if (count($array) > 0)
				$result = array_merge($result, array($array));
			
			return $result;
		}

		public static function rsearch($haystack, $needle)
		{
	    foreach ( $haystack as $key => $value )
        $current_key = $key;
        if ( $needle === $value
        	OR ( is_array($value)
        		&& self::rsearch($haystack, $needle) !== false ) )
            return $current_key;
	    return false;
		}

		public static function pluck(&$array, $key)
		{
			$element = $array[$key];
			unset($array[$key]);
			return $element;
		}

		public static function unshift(&$array, $element, $key=null)
		{
			if ( !$key )
				return array_unshift($array, $element);
			else
				$array = array($key => $element) + $array;
				return count($array);			
		}

		public static function sortObjects(&$array, $property)
		{
			$new_array = array();
			foreach ( $array as $object )
				$new_array[$object->$property] = $object;
			ksort($new_array);
			$array = $new_array;
		}

		// Iterates over $array removing every object with the same value for
		// $property after the first occurence.
		public static function filterUniqueParameter(&$array, $property)
		{
			$values = array();
			foreach ( $array as $key => $object )
			{
				if ( in_array($object->$property, $values) )
					unset($array[$key]);
				else
					$values[] = $object->$property;
			}

		}

		/**
		 * Utility that returns $singular or $plurar based on the $array count.
		 *
		 * @param string $singular
		 * @param string $plural
		 * @param array $array
		 * @return string
		 **/
		public static function plural($singular, $plural, $array)
		{
			if ( count($array) === 1 ) return $singular;
			else return $plural;
		}

		/**
		 * Receives an array of objects and returns a new associative array
		 * where the key is the specified property of each object in the array
		 *
		 * @param array $arr
		 * @param string $prop
		**/
		public static function propToKey($arr, $prop)
		{
			$newArr = array();
			foreach ( $arr as $el ) {
				$newArr[$el->{$prop}] = $el;
			}
			return $newArr;
		}

		/**
		 * Receives an array of objects and returns a new associative array
		 * where the keys and values are the specified object properties
		 *
		 * @param array $arr
		 * @param string $keyProp
		 * @param string $valueProp
		**/
		public static function mapObjects($arr, $keyProp, $valueProp)
		{
			$newArr = array();
			foreach ( $arr as $el ) {
				$newArr[$el->$keyProp] = $el->$valueProp;
			}
			return $newArr;
		}
		
	}
?>
