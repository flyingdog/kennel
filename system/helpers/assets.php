<?php
	class assets
	{
		private static $_CACHE;
		const RETURN_URL = 1;
		const RETURN_PATH = 2;

		static function img($uri)
		{
			# Absolute paths
			if (substr($uri, 0, 7) === 'http://')
				return $uri;

			# Cascading resource
			$path = cascade::img($uri, true);
			if ( $path ) return $path;
			else debug::error("assets helper: Image <b>{$uri}</b> not found.");
		}

		static function css($uri)
		{
			# Absolute paths
			if (substr($uri, 0, 7) === 'http://')
				return $uri;

			# Cascading resource
			$path = cascade::css($uri, true);
			if ( $path ) return $path;
			else debug::error("assets helper: CSS <b>{$uri}</b> not found.");
		}

		static function js($uri)
		{
			# Absolute paths
			if (substr($uri, 0, 7) === 'http://')
				return $uri;

			# Cascading resource
			$path = cascade::js($uri, true);
			if ( $path ) return $path;
			else debug::error("assets helper: JS <b>{$uri}</b> not found.");
		}

		static function video($uri)
		{
			# Absolute paths
			if (substr($uri, 0, 7) === 'http://')
				return $uri;

			# Cascading resource
			$path = cascade::video($uri, true);
			if ( $path ) return $path;
			else debug::error("assets helper: Video <b>{$uri}</b> not found.");
		}

		static function file($uri)
		{
			# Absolute paths
			if (substr($uri, 0, 7) === 'http://')
				return $uri;

			# Cascading Resource
			$path = cascade::file($uri, true);
			if ( $path ) return $path;
			else debug::error("assets helper: File <b>{$uri}</b> not found.");
		}

	}
?>
