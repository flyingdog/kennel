<?php
	class auth
	{
		static $user = array();
		static $error;
		static $message;

		/**
		* Attempt to login the user returning a boolean representing
		* authentication success
		*
		* @param string $identifier - the identifier
		* @param string $password - the plain password
		* @param string $realm - the authentication realm (default "admin")
		*
		* @return boolean
		*/
		static function login($identifier, $password, $realm='admin')
		{
			$model_name = Kennel::getSetting('auth', 'model_name');
			$identifier_field = Kennel::getSetting('auth', 'identifier_field');
			$password_field = Kennel::getSetting('auth', 'password_field');
			$encryption_field = Kennel::getSetting('auth', 'encryption_field');

			$user = Crit($model_name)->add($identifier_field, $identifier)->retrieveFirst();

			// Identifier not found in database
			if (!$user) {
				return false;
			}

			$success = null;

			switch ($user->{$encryption_field}) {
				case "blowfish":
				case "bcrypt":
					$success = self::verifyBlowfishHash($password, $user->{$password_field}, $realm);
					break;
				case "md5":
				default:
					$success = self::verifyMD5Hash($password, $user->{$password_field}, $realm);
					break;
			}

			if ($success) {
				self::$user[$realm] = $user;
				if( !session_id() ) session_start();
				$app_id = Kennel::getSetting('application', 'id');
				$_SESSION["{$app_id}-{$realm}"] = self::$user[$realm]->toArray();
				return true;
			}

			return false;
		}

		static function hashPassword($password)
		{
			$hasher = new PasswordHash(8, false);
			return $hasher->HashPassword($password);
		}

		static function verifyBlowfishHash($password, $hash, $realm='admin')
		{
			$hasher = new PasswordHash(8, false);
			return $hasher->CheckPassword($password, $hash);
		}

		private static function verifyMD5Hash($password, $hash, $realm='admin')
		{
			return md5($password) === $hash;
		}

		/**
		* @returns boolean
		* @param string $realm
		*/
		static function logout($realm='admin')
		{
			if(!session_id()) session_start();

			$app_id = Kennel::getSetting('application', 'id');
			unset($_SESSION["{$app_id}-{$realm}"]);

			return true;
		}

		static function gtfo()
		{
			header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden'); die();
		}

		/**
		* boolean check( [string $realm, [ $level, $level2 ... ]] )
		*
		* Checks for an authenticated user and whether that user's access_level
		* matches one of the levels passed as arguments.
		*
		* Realm is optional, and defaults to 'admin' if ommited. If the first
		* argument is a non-numeric string, that string is used as the realm.
		*
		* TODO: $realm should be the last argument to maintain consistency
		*/
		static function check()
		{
			if ( !session_id() ) session_start();

			$args = func_get_args();

			// Realm is optional; if present, should be the first argument,
			// non-numeric, followed by n numeric access_level arguments

			if ( count($args) > 0 && !is_numeric($args[0]) )
			  $realm = array_shift($args);
			else
  			$realm = 'admin';

			$app_id = Kennel::getSetting('application', 'id');

			// No realm information in the session, nothing to do here...
			if ( !array_key_exists("{$app_id}-{$realm}", $_SESSION) )
			  return false;

			$user = self::getUser($realm);
			if ( count($args) > 0 ) {

  				$userlevel_field = Kennel::getSetting('auth', 'userlevel_field');

  				foreach ($args as $arg) {
  					if ( $arg === (int) $user->$userlevel_field )
  					  return true;
  				}
  				return false;
			} else {
				return true;
			}

			return false;
		}

		/**
		 * @param string $realm
		 * @return Model
		 */
		static function getUser($realm='admin')
		{
			$app_id = Kennel::getSetting('application', 'id');

			if ( !session_id() ) session_start();

			if ( array_key_exists($realm, self::$user) )
			{
				return self::$user[$realm];
			}
			elseif ( isset($_SESSION["{$app_id}-{$realm}"]) )
			{
			  $model_name = Kennel::getSetting('auth', 'model_name');
				self::$user[$realm] = Model::instance($model_name);
				self::$user[$realm]->fromArray($_SESSION["{$app_id}-{$realm}"], true);
				return self::$user[$realm];
			}
			else
				return false;
		}

		/**
		 * @param Model $user
		 * @param string $realm
		 */
		static function updateUser($user, $realm='admin')
		{
			if(!session_id()) session_start();

			$app_id = Kennel::getSetting('application', 'id');
			$_SESSION["{$app_id}-{$realm}"] = $user->toArray();
			self::$user[$realm] = $user;
		}

	}
?>
