<?php
	class HttpResponse {
		var $length;
		var $contentType;
		var $status;
		var $time;
		var $text;
	}

	class http
	{
		/** 
		 * Send a GET requst using cURL 
		 * @param string $url to request 
		 * @param array $data values to send 
		 * @param array $options for cURL 
		 * @return string 
		 */ 
		static function get($url, array $data = array(), array $options = array())
		{
			$defaults = array( 
					CURLOPT_URL => $url. (strpos($url, '?') === FALSE ? '?' : ''). http_build_query($data), 
					CURLOPT_HEADER => 0, 
					CURLOPT_RETURNTRANSFER => TRUE, 
					CURLOPT_TIMEOUT => 4 
			); 
	
			$ch = curl_init(); 
			curl_setopt_array($ch, ($options + $defaults)); 
			if( ! $result = curl_exec($ch)) trigger_error(curl_error($ch)); 
			curl_close($ch); 
			return $result; 
		}

		/** 
		 * Send a POST requst using cURL 
		 * @param string $url to request 
		 * @param array $data values to send 
		 * @param array $options for cURL 
		 * @return string 
		 */ 
		static function post($url, array $data=array(), array $options=array(), &$responseStatus=null)
		{
			$defaults = array( 
					CURLOPT_URL => $url,
					CURLOPT_HEADER => 0,
					CURLOPT_RETURNTRANSFER => TRUE,
					CURLOPT_POSTFIELDS => http_build_query($data),
					CURLOPT_TIMEOUT => 4,
			); 
	
			$ch = curl_init();
			curl_setopt_array($ch, ($options + $defaults));
			if (!$resultText = curl_exec($ch)) {
				trigger_error(curl_error($ch));
			}

			$result = new HttpResponse();
			$result->length = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
			$result->contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
			$result->status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			$result->time = curl_getinfo($ch, CURLINFO_TOTAL_TIME);
			$result->text = $resultText;

			curl_close($ch);

			return $result;
		}
		
		static function xml($url)
		{
			$xml = self::get($url);
			
			$xmldoc = new DOMDocument('1.0');
			$xmldoc->loadXML($xml);
			
			return $xmldoc;
		}
		
		static function json($url)
		{
		  $json = self::get($url);
		  
		  return json::decode($json);
		}
	}
?>
