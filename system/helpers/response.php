<?php
	class response
	{
		private static $HTTP_CODES = array(
			100 => 'Continue',
			101 => 'Switching Protocols',
			102 => 'Processing',

			200 => 'Ok',
			201 => 'Created',
			202 => 'Accepted',
			203 => 'Non-Authoritative Information', // HTTP/1.1
			204 => 'No Content',
			205 => 'Reset Content',
			206 => 'Partial Content',
			207 => 'Multi-Status',
			208 => 'Already Reported',
			226 => 'IM Used',


			301 => 'Moved Permanently',
			302 => 'Found',
			303 => 'See Other',
			304 => 'Not Modifed',
			305 => 'Use Proxy', // HTTP/1.1
			307 => 'Temporary Redirect', // HTTP/1.1
			308 => 'Permanent Redirect',

			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			406 => 'Not Acceptable',
			407 => 'Proxy Authentication Required',
			408 => 'Request Timeout',
			409 => 'Conflict',
			410 => 'Gone',
			411 => 'Length Required',
			412 => 'Precondition Failed',
			413 => 'Request Entity Too Large',
			414 => 'Request-URI Too Long',
			415 => 'Unsuported Media Type',
			416 => 'Requested Range Not Satisfiable',
			417 => 'Expectation Failed'
		);

		public static function status($status=200)
		{
			header("HTTP/1.1 {$status} " . self::$HTTP_CODES[$status]);
		}

		public static function json($data, $status=200)
		{
			header("HTTP/1.1 {$status} " . self::$HTTP_CODES[$status]);
			header('content-type: application/json; charset:UTF-8');
			echo json::encode($data);
			exit();
		}
	}
?>