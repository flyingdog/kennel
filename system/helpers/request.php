<?php
	
	class request {
		public static $METHOD;
		public static $REQUEST_URL;
		public static $PROTOCOL;
		public static $PARAMETERS = array();
		public static $ACTION;
		public static $USER_DATA;
		public static $CONTROLLER;
		public static $PATH;

		/**
		 * Checks if the request comes from an Ajax (XMLHttpRequest) call.
		 * 
		 * This methods relies on the client implementation compliance to the
		 * standard, so false negatives might happen.
		 * 
		 * @return boolean
		 **/
		static function isAjax()
		{
			if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
			strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
				return true;
			else
				return false;
		}

		/**
		 * Attempts to verify wether the request came from a command line call
		 * instead of a regular HTTP request.
		 *
		 * @return boolean
		 **/
		static function isCli()
		{
			return !array_key_exists('HTTP_HOST', $_SERVER);
		}
	
		
		/**
		 * Returns the request referer
		 *
		 * @param string
		 **/
		static function referer()
		{
			return $_SERVER['HTTP_REFERER'];
		}

		/**
		 * Wether or not the request method is POST.
		 *
		 * @return boolean
		 **/
		static function isPost()
		{
			return strtoupper($_SERVER['REQUEST_METHOD']) == 'POST';
		}

		/**
		 * Redirects the request the given $location.
		 * 
		 * If the $location string begins with a valid URL scheme, the redirect is
		 * made to that URL, otherwise passes $location through url() first. As
		 * such, location can be simply a path string and is the preferred method
		 * to issue redirects within the same domain.
		 *
		 * @param string location
		 * @param int status
		 **/
		static function redirect($location, $status=null)
		{
		  // switch ($status)
		  // {
		  //   case 301: header("Status: 301 Moved Permanently");
		  //   case 302: header("Status: 302 Found");
		  //   case 303: header("Status: 303 See Other");
		  //   case 307: header("Status: 307 Temporary Redirect");
		  // }
		  
		  $scheme = parse_url($location, PHP_URL_SCHEME);
		  if ( !$scheme )
  		  header("Location: " . url($location));
      else
  		  header("Location: " . $location);
  		
  		exit(0);
		}

		/**
		 * Declares a 404 HTTP status and calls the main:notfound router action
		 **/
		static function notfound()
		{
			header('HTTP/1.0 404 Not Found');
			Controller::call('get', 'main:notfound');
			exit(0);
		}
	
		/**
		 * Prints out a detailed description of the request.
		 **/
		static function dump($return=false)
		{
			$style = html::css('kennel-debug.css');
			$table = XML::element('table', null, array(
				'class'=>'kennel-debug',
				'border'=>'1'
			));

			$tr = XML::element('tr', $table);
			XML::element('th', $tr, array('colspan'=>2), 'Request');

			$tr = XML::element('tr', $table);
			XML::element('th', $tr, null, 'method');
			XML::element('td', $tr, null, self::$METHOD);

			$tr = XML::element('tr', $table);
			XML::element('th', $tr, null, 'url');
			XML::element('td', $tr, null, self::$REQUEST_URL);

			$tr = XML::element('tr', $table);
			XML::element('th', $tr, null, 'path');
			XML::element('td', $tr, null, self::$PATH);

			$tr = XML::element('tr', $table);
			XML::element('th', $tr, array('colspan'=>2), 'Matched Route');
			
			$tr = XML::element('tr', $table);
			XML::element('th', $tr, null, 'action');
			XML::element('td', $tr, null, self::$ACTION);

			$tr = XML::element('tr', $table);
			XML::element('th', $tr, null, 'parameters');
			XML::element('td', $tr, null, debug::getDump(self::$PARAMETERS, true));
			
			$tr = XML::element('tr', $table);
			XML::element('th', $tr, null, 'path');
			XML::element('td', $tr, null, cascade::controller(self::$CONTROLLER));

			$tr = XML::element('tr', $table);
			XML::element('th', $tr, null, 'userData');
			XML::element('td', $tr, null, self::$USER_DATA);

			if ( $return ) return $style.$table;
			else echo $style.$table;
		}
		
		/**
		 * Resolves the path portion relevant to the application, which is the
		 * portion following the root application path.
		 *
		 * @param string $url
		 * @return string
		 **/
		static function path($url)
		{
			// Parse out the requested URI
			if(Kennel::getSetting('application', 'use_mod_rewrite'))
				$path_string = substr(rtrim($url, '/'), strlen(Kennel::$ROOT_URL));
			else
				$path_string = substr(rtrim($url, '/'), strlen(Kennel::$ROOT_URL . '/index.php'));

			$uri = '/' . ltrim(str_replace(strstr($path_string, '?'), '', $path_string), '/');

			// Cleanup the segments to avoid injection attacks
			$uri_segments = explode('/', $uri);
			foreach ( $uri_segments as $key=>$segment )
				if ( strlen($segment) )
					$uri_segments[$key] = input::clean($segment);
				else
					unset($uri_segments[$key]);

			if ( count($uri_segments) > 0 )
				return '/' . implode('/', $uri_segments) . '/';
			else
				return '/';
		}

		/**
		 * Returns the value of the route parameter $key in the current request.
		 *
		 * @return string
		 **/
		public static function param($key)
		{
			if ( isset(self::$PARAMETERS[$key]) )
				return self::$PARAMETERS[$key];
			return null;
		}

		/**
		 * This method is invoked by the Kennel initialization process and
		 * should not be called elsewhere.
		 **/
		static function process()
		{
			// Set the method
			self::$METHOD = strtoupper($_SERVER['REQUEST_METHOD']);

			// Set the protocol
			self::$PROTOCOL = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https" : "http";

			// Get the request parts
			$scheme = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? 'https' : 'http';
			self::$REQUEST_URL = $scheme . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

			self::$PATH = self::path(self::$REQUEST_URL);

			$route = router::resolve(self::$METHOD, self::$PATH, $parameters, $route);

			// Enforce HTTPS setting
			if ( Kennel::getSetting('application', 'enforce_https') && strtolower($scheme) !== 'https' )
			{
				request::redirect('https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
			}

			if ( $route )
			{
				// If the request matches a route, populate the request data
				self::$PARAMETERS = $parameters;
				self::$CONTROLLER = strtok($route["action"], ':');
				self::$ACTION = $route["action"];
				self::$USER_DATA = $route["userData"];

				// And call the action
				Controller::call(self::$METHOD, $route["action"], $parameters);
			}
			else
			{
				// Else, call the default not found method
				Controller::call('get', 'main:notfound');

				// If the application:debug_mode setting is true, also logs the request
				if ( Kennel::getSetting('application', 'debug_mode') )
					error_log('404: ' . self::$METHOD . ' ' . self::$PATH);
			}
		}

		/**
		 * Returns the IP from which the request was sent.
		 *
		 * @return string the client IP
		 **/
		public static function ip() {
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) // share
			{
				return $_SERVER['HTTP_CLIENT_IP'];
			}
			elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  // proxy
			{
				return $_SERVER['HTTP_X_FORWARDED_FOR'];
			}
			else // direct
			{
				return $_SERVER['REMOTE_ADDR'];
			}
		}

		/**
		 * Returns the user data associated with the matched route
		 * 
		 * @param $arrKey if userData is an array, returns the value for key $arrKey, otherwise this parameter is ignored
		 * @return misc userData
		 */
		public static function routeData($arrKey=null) {
			if (is_array(self::$USER_DATA) && !is_null($arrKey)) {
				return self::$USER_DATA[$arrKey];
			}
			return self::$USER_DATA;
		}
		
	}
	
?>
