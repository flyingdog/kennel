<?php
	
	class html
	{
		
		static function anchor($url, $text, $properties=null)
		{
			if(!$properties) $properties = array();
			$properties['href'] = $url;
			
			return $a = XML::element('a', null, $properties, $text);
		}
		
		static function meta($properties)
		{
			$meta = XML::element('meta', null, $properties);
			$meta->self_closing = XML::SELF_CLOSING_HTML;
			return $meta;
		}
		
		static function link($rel, $href, $type, $title=null)
		{
			$link = XML::element('link');
			$link->rel = $rel;
			$link->href = $href;
			$link->self_closing = XML::SELF_CLOSING_HTML;
			if ($link) $link->type = $type;
			if ($title) $link->title = $title;
			return $link;
		}
		
		static function favicon($img)
		{
			return self::link('shortcut icon', assets::img($img), 'image/png');
		}
		
		static function css()
		{
			$arguments = func_get_args();
			$arguments = a::flatten($arguments);
			if (sizeOf($arguments) == 0) return null;
			
			$link = XML::element('style');
			$link->type = 'text/css';
			foreach ($arguments as $stylesheet)
			{
				$parts = explode('?', $stylesheet);
				$filePath = $parts[0];
				$version = array_key_exists(1, $parts) ? '?' . $parts[1] : '';
				$importPath = assets::css($filePath) . $version;
				$text = new XMLText("\n\t@import url('{$importPath}');", $link);
			}
			return $link->output();
		}
		
		static function js()
		{
			$arguments = func_get_args();
			$arguments = a::flatten($arguments);
			if (sizeOf($arguments) == 0) return null;
			
			$return = '';
			foreach ($arguments as $script)
			{
				$parts = explode('?', $script);
				$filePath = $parts[0];
				$version = array_key_exists(1, $parts) ? '?' . $parts[1] : '';
				$srcPath = assets::js($filePath) . $version;

				$script = XML::element('script');
				$script->type = 'text/javascript';
				$script->src = $srcPath;
				$script->adopt(XML::text(''));
				$return .= "\n\t".$script->output();
			}
			return $return;
		}
		
		static function img($filename, $alt_or_properties=null, $title=null)
		{
			$img = XML::element('img');

			if (is_array($alt_or_properties))
			{
				foreach ($alt_or_properties as $key=>$value)
				{
					$img->$key = $value;
				}
			}
			else if (is_string($alt_or_properties))
			{
				$img->alt = $alt_or_properties;
			}

			if (!$img->alt) $img->alt = $filename;

			$img->src = assets::img($filename);
			
			if ($title) $img->title = $title;
			return $img;
		}
		
	}
	
?>
