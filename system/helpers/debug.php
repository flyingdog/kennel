<?php
	class debug
	{
		const NOTICE = E_USER_NOTICE;
		const WARNING = E_USER_WARNING;
		const ERROR = E_USER_ERROR;

		static $TIME_INIT = null;
		static $TIME_LAP = null;

		/**
		 * Inits the execution timer.
		 **/
		static function init()
		{
			self::$TIME_INIT = microtime(true);
		}

		/**
		 * Returns the current execution time.
		 * 
		 * The time is a delta between the current and the one from the init() call.
		 *
		 * @return float
		 **/
		static function timeDeltaFrom($time)
		{
			//finish the benchmark
			return round(microtime(true) - $time, 4);
		}

		/**
		 * Prints out a human readable table with current execution time,
		 * number of database queries executed and memory usage.
		 **/
		static function benchmark()
		{
			$style = html::css('kennel-debug.css');
			$table = XML::element('table', null, array('class'=>'kennel-debug'));

			$tr = XML::element('tr', $table);
			XML::element('th', $tr, null, 'Queries:');
			XML::element('td', $tr, null, MySQL::numQueries());

			$tr = XML::element('tr', $table);
			XML::element('th', $tr, null, 'Elapsed time:');
			XML::element('td', $tr, null, self::timeDeltaFrom(self::$TIME_INIT));

			if ( self::$TIME_LAP )
			{
				$tr = XML::element('tr', $table);
				XML::element('th', $tr, null, 'Since last:');
				XML::element('td', $tr, null, self::timeDeltaFrom(self::$TIME_LAP));
			}

			$tr = XML::element('tr', $table);
			XML::element('th', $tr, null, 'Using:');
			XML::element('td', $tr, null, self::memory_usage());

			$tr = XML::element('tr', $table);
			XML::element('th', $tr, null, 'Peak memory usage::');
			XML::element('td', $tr, null, self::peak_memory_usage());
			
			echo $style.$table;

			self::$TIME_LAP = microtime(true);
		}

		/**
		 * Returns the current memory usage from the application, in a human
		 * readable format.
		 * 
		 * @return int
		 **/
		static function memory_usage()
		{
			$size = memory_get_usage(true);
	    return format::size($size);
		}


		/**
		 * Returns the peak memory usage from the application in a human readable
		 * format.
		 * 
		 * @return int
		 **/
		static function peak_memory_usage()
		{
			$size = memory_get_peak_usage(true);
	    return format::size($size);
		}

		/**
		 * Accepts an arbitrary number of arguments, and prints a table with dumps
		 * for each.
		 **/
		static function dump(/*. args .*/)
		{
			$args = func_get_args();

			$style = html::css('kennel-debug.css');

			$backtrace = debug_backtrace();

			$table = XML::element('table', null, array('class'=>'kennel-debug'));
			$tr = XML::element('tr', $table);
			XML::element('th', $tr, null, $backtrace[0]['file'] . ' : ' . $backtrace[0]['line']);

			foreach ( $args as $arg )
			{
				$tr = XML::element('tr', $table);
				$td = XML::element('td', $tr);
				XML::element('pre', $td, null, self::getDump($arg, true));
			}
			
			echo $style . $table;
		}
		
		/**
		 * Generates and returns a dump of the passed parameter, wraped in a pre
		 * tag. Used by the various dump methods in the framework.
		 *
		 * @param mixed $variable
		 **/
		static function getDump($variable, $pre_wrap=false)
		{
			ob_start();
			var_dump($variable);
			$raw_dump = ob_get_contents();
			ob_end_clean();
			if (!$pre_wrap) return $raw_dump;
			return XML::element('pre', null, null, $raw_dump);
		}
		
		/**
		 * Prints out a nicely formatted bactrace.
		 *
		 * @param int $depth How many steps from the begining of the backtrace to omit.
		 * @param int $limit Maximum number of steps to print out.
		 * @param boolean $detailed Display function arguments and object data
		 **/
		static function backtrace($depth=0, $limit = 100, $detailed=false)
		{
			$full_backtrace = debug_backtrace();
			
			$table = XML::element('table', null, array('class'=>'kennel-debug'));

			$tr = XML::element('tr', $table);
			$th = XML::element('th', $tr, array('colspan'=>5), "Backtrace");
			XML::element('small', $th, null, $full_backtrace[0]['file'] . ' : ' . $full_backtrace[0]['line']);
			
			$tr = XML::element('tr', $table);
			$th = XML::element('th', $tr, null, 'class');
			$th = XML::element('th', $tr, null, '');
			$th = XML::element('th', $tr, null, 'function');
			$th = XML::element('th', $tr, null, 'file');
			$th = XML::element('th', $tr, null, 'line');

			for($n = $depth, $i=0; $n < count($full_backtrace); $n++, $i++)
			{
				if ($limit === $i) break;
				$backtrace = $full_backtrace[$n];
				// debug::dump($backtrace);
				$tr = XML::element('tr', $table);
				
				if (isset($backtrace['class'])) XML::element('td', $tr, null, $backtrace['class']);
				else XML::element('td', $tr);

				if (isset($backtrace['type']))  XML::element('td', $tr, null, $backtrace['type']);
				else XML::element('td', $tr);
				
				if (isset($backtrace['function'])) XML::element('td', $tr, null, $backtrace['function']);
				else XML::element('td', $tr);
				
				if (isset($backtrace['file'])) XML::element('td', $tr, null, $backtrace['file']);
				else XML::element('td', $tr);
				
				if (isset($backtrace['line'])) XML::element('td', $tr, null, $backtrace['line']);
				else XML::element('td', $tr);

				if ($detailed)
				{
					$tr = XML::element('tr', $table);

					$td = XML::element('td', $tr, array('colspan'=>3));
					XML::element('pre', $td, array('class'=>'kennel-collapsible'), self::getDump($backtrace['args'], false));

					$td = XML::element('td', $tr, array('colspan'=>2));
					if ( isset($backtrace['object']) )
					{
						XML::element('pre', $td, array('class'=>'kennel-collapsible'), self::getDump($backtrace['object'], false));
						XML::element('button', $td, array('class'=>'kennel-toggle-collapse'), 'toggle');
					}
				}
			}

			$style = html::css('kennel-debug.css');
			$script = html::js('kennel-debug.js');
			
			echo $style.$table.$script;
		}
		
		public static function whence($level=2)
		{
			$backtrace = debug_backtrace();
			return array(
				'file' => $backtrace[$level]['file'],
				'line' => $backtrace[$level]['line'],
				'call' => $backtrace[$level]['class'] . '::' . $backtrace[$level]['function'],
				'from' => $backtrace[$level-1]['class'] . '::' . $backtrace[$level-1]['function']
				);
		}
		
		static function error($string, $context=null, $backtrace_depth=1)
		{
			$backtrace = debug_backtrace();
			$step = $backtrace[$backtrace_depth];
			$file = isset($step['file']) ? $step['file'] : null;
			$line = isset($step['line']) ? $step['line'] : null;
			error_log($string);
			self::errorHandler(E_USER_ERROR, $string, $file, $line, $context);
			die();
		}

		static function warning($string, $context=null, $backtrace_depth=1)
		{
			$backtrace = debug_backtrace();
			$step = $backtrace[$backtrace_depth];
			$file = isset($step['file']) ? $step['file'] : null;
			$line = isset($step['line']) ? $step['line'] : null;
			self::errorHandler(E_USER_WARNING, $string, $file, $line, $context);
		}
		
		static function log($message)
		{
		  $path = Kennel::$ROOT_PATH . '/application/assets/files/log.txt';
		  file_put_contents($path, $message, FILE_APPEND);
		}

		/**
		 * Default error handler. Not to be called directly by the user.
		 *
		 * @param int $number
		 * @param string $string
		 * @param string $file
		 * @param int $line
		 * @param array $context
		 **/
		 static function errorHandler($number, $string, $file=null, $line=null, $context=null)
		 {
			// This error code is not included in error_reporting
			if ( !(error_reporting() & $number) ) return;
			
			if ( request::isCli() )
			{
				$default = "\033[0;0m"; $red = "\033[31m"; $cyan = "\033[36m";

				// Cli call
				$type = self::errorType($number);
				echo "{$red}{$type}:{$default}\n{$string}\n{$cyan}{$file} : {$line}{$default}\n\n";
			}
			elseif ( request::isAjax() )
			{	
				// Ajax request
				$type = self::errorType($number);
				echo "{$type}:\n{$string}\n{$file} : {$line}";
			}
			else
			{
				// HTTP request
				$table = XML::element('table', null, array('class'=>'kennel-debug'));

				$ignore = array(E_STRICT);

				$tr = XML::element('tr', $table);
				XML::element('th', $tr, array('colspan'=>2), self::errorType($number));

				$tr = XML::element('tr', $table);
				XML::element('th', $tr, null, 'String');
				XML::element('td', $tr, null, $string);

				if ( $file )
				{
					$tr = XML::element('tr', $table);
					XML::element('th', $tr, null, 'File');
					XML::element('td', $tr, null, $file);
				}

				if ( $line )
				{
					$tr = XML::element('tr', $table);
					XML::element('th', $tr, null, 'Line');
					XML::element('td', $tr, null, $line);
				}

				if ( $context && Kennel::getSetting('application', 'debug_context') )
				{
					$tr = XML::element('tr', $table);
					XML::element('th', $tr, null, 'Context');
					$td = XML::element('td', $tr);
					XML::element('pre', $td, array('class'=>'kennel-collapsible'), self::getDump($context, false));
					XML::element('button', $td, array('class'=>'kennel-toggle-collapse'), 'toggle');
				}

				$style = html::css('kennel-debug.css');
				$script = html::js('kennel-debug.js');

				echo $style . $table . $script;
				self::backtrace(2);
			}
		}

		static function errorType($number)
		{
			switch ($number)
			{
				case E_ERROR: return 'Fatal Error';
				case E_WARNING: return 'Warning';
				case E_PARSE: return 'Parse Error';
				case E_NOTICE: return 'Notice';
				
				case E_USER_ERROR: return 'Fatal Error (user)';
				case E_USER_WARNING: return 'Warning (user)';
				case E_USER_NOTICE: return 'Notice (user)';

				case E_STRICT: return 'Strict Error';
				case E_RECOVERABLE_ERROR: return 'Recoverable Fatal Error';
				case E_DEPRECATED: return 'Deprecated';
				case E_USER_DEPRECATED: return 'Deprecated (user)';

				default: return 'Error';
			}
		}

		static function livereload() {
			$view = new View('livereload');
			$view->render();
		}
	}

	set_error_handler(array('debug', 'errorHandler'));
?>
