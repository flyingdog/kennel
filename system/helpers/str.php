<?php
	class str
	{
		/**
		 * Truncates $string at $limit, concatenated with $append. If truncating
		 * exactly at $limit would cut a word in the middle, truncates before that
		 * word.
		 *
		 * @param string $string
		 * @param int $limit
		 * @param string $append
		 **/
		static function truncate($string, $limit=50, $append='&hellip;')
		{
			if (strlen($string) <= $limit)
				return $string;

			$return = substr($string, 0, $limit);

			if (strpos($string,' ') === FALSE)
				return "{$return}{$append}";

			return preg_replace('/[^\s]+$/', '', $return) . $append;
		}
		
		/**
		 * Transliterate characters into simple ASCII, "removing accents".
		 * 
		 * @param string $string
		 * @return string
		**/
		function toAscii($string)
		{
			return strtr(utf8_decode($string), 
				utf8_decode(
				'ŠŒŽšœžŸ¥µÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïðñòóôõöøùúûüýÿ'),
				'SOZsozYYuAAAAAAACEEEEIIIIDNOOOOOOUUUUYsaaaaaaaceeeeiiiionoooooouuuuyy');
		}
		
		/**
		 * Returns true if $suffix is present at the end of $string.
		 *
		 * @param string $string
		 * @param string $suffix
		 * @return bool
		 **/
		static function endsWith($string, $suffix)
		{
			$len = strlen($suffix);
			return substr($string, strlen($string) - $len, $len) === $suffix;
		}

		/**
		 * Returns true if $prefix is present at the begining of $string.
		 *
		 * @param string $string
		 * @param string $prefix
		 * @return bool
		 **/
		static function beginsWith($string, $prefix)
		{
			return substr($string, 0, strlen($prefix)) === $prefix;
		}

		/**
		 * Accepts a dash-separated string and returns a camelCased string
		 *
		 * @param string $string
		 * @return string
		 **/
		static function camelCase($string)
		{
			$words = explode('-', $string);
			$camelCase = '';
			foreach ($words as $n => $word)
			{
				if ($n === 0) $camelCase .= strtolower($word);
				else $camelCase .= ucfirst(strtolower($word));
			}
			return $camelCase;
		}

		/**
		 * Accepts a dash-separated string and returns a PascalCased string
		 *
		 * @param string $string
		 * @return string
		 **/
		static function pascalCase($string)
		{
			$words = explode('-', $string);
			$pascalCase = '';
			foreach ($words as $n => $word)
			{
				$pascalCase .= ucFirst(strtolower($word));
			}
			return $pascalCase;
		}

		/**
		 * Accepts a dash-separated or another provided separator string and returns
		 * Title Case
		 *
		 * @param string $string
		 * @return string
		 **/
		static function titleCase($string, $separator="-")
		{
			$words = explode($separator, $string);
			$titleCase = '';
			foreach ($words as $n => $word)
			{
				$titleCase .= $n > 0 ? ' ' : '';
				$titleCase .= ucFirst(strtolower($word));
			}
			return $titleCase;
		}
		
		/**
		 * Utility that returns $singular or $plurar based on the $number argument
		 *
		 * @param string $singular
		 * @param string $plural
		 * @param numeric $number
		 * @return string
		 **/
		public static function plural($singular, $plural, $number)
		{
			if ( intval($number) === 1 ) return $singular;
			else return $plural;
		}
	}
?>
