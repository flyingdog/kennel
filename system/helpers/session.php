<?php
  /**
   * The `session` helper allows easy access to session variables
   */
  class session
  {
    /**
     * Sets a session value to a given key
     * 
     * @param string $key the key to set the value to
     * @param string $value the value to set
     */
    static function set($key, $value)
    {
			if(!session_id()) session_start();
      $app_id = Kennel::getSetting('application', 'id');
      
      if ( !array_key_exists("{$app_id}-session", $_SESSION) )
        $_SESSION["{$app_id}-session"] = array();

      $_SESSION["{$app_id}-session"][$key] = $value;
    }

    /**
     * Deletes the key/value pair from the session
     *
     * @param string $key
     **/
    static function delete($key)
    {
      if(!session_id()) session_start();
      $app_id = Kennel::getSetting('application', 'id');

      if ( array_key_exists("{$app_id}-session", $_SESSION)
        &&  array_key_exists($key, $_SESSION["{$app_id}-session"]) )
        unset($_SESSION["{$app_id}-session"][$key]);
    }
    
    /**
     * Gets a session value from a given key
     * 
     * @param string $key the key to retrieve the value from
     */
    static function get($key)
    {
			if(!session_id()) session_start();
      $app_id = Kennel::getSetting('application', 'id');
      
      if ( array_key_exists("{$app_id}-session", $_SESSION)
        && array_key_exists($key, $_SESSION["{$app_id}-session"]) )
        return $_SESSION["{$app_id}-session"][$key];
      else
        return null;
    }
    
    /**
     * Checks whether the given key exists in the session
     * 
     * @param string $key the key to check
     */
    static function exists($key)
    {
			if(!session_id()) session_start();
      $app_id = Kennel::getSetting('application', 'id');
      
      if ( array_key_exists("{$app_id}-session", $_SESSION)
        &&  array_key_exists($key, $_SESSION["{$app_id}-session"]) )
        return true;
      else
        return false;
    }

    /**
     * Prints all the information stored in the session
     **/
    static function dump()
    {
      if(!session_id()) session_start();
      $app_id = Kennel::getSetting('application', 'id');

      if ( array_key_exists("{$app_id}-session", $_SESSION) )
      {
        debug::dump($_SESSION["{$app_id}-session"]);
      }
    }
    
  }
?>