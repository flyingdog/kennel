<?php
	class router
	{
		/**
		 * All registered routes are stored here, under the correct method.
		 **/
		public static $ROUTES = array(
			'GET'=>array(),
			'POST'=>array(),
			'PUT'=>array(),
			'DELETE'=>array(),
			'PATCH'=>array(),
			'HEAD'=>array(),
			'OPTIONS'=>array()
		);

		const OPTIONAL_PLACEOLDER = '/\/?:[a-zA-Z0-9_]+\?/';
		const REQUIRED_PLACEHOLDER = '/\/?:[a-zA-Z0-9_]+/';
		const WILDCARD_PLACEHOLDER = '/\/?\*/';

		/**
		 * Array with regular expressions to replace the route parameter
		 * placeholders.
		 **/
		private static $_PLACEHOLDERS = array(
			self::REQUIRED_PLACEHOLDER => "(/[a-zA-Z0-9\.\-_]+)",
			self::OPTIONAL_PLACEOLDER => "(/[a-zA-Z0-9\.\-_]+)?",
			self::WILDCARD_PLACEHOLDER => '(.*)',
		);

		/**
		 * Register a $route with $action for GET requests.
		 *
		 * Example:
		 *
		 *	// The following route:
		 *	router::get('/admin/manage-users/edit-user/:id', 'admin/manage-users:edit-user')
		 *
		 *	// Resolves to this method call:
		 *	Admin_ManageUsers_controller::get_editUser($id)
		 *
		 * @return void
		 **/
		public static function get($route, $action, $userData=null)
		{
			self::register('GET', $route, $action, $userData);
		}

		/**
		 * Register a $route with $action for POST requests.
		 *
		 * @return void
		 **/
		public static function post($route, $action, $userData=null)
		{
			self::register('POST', $route, $action, $userData);
		}

		/**
		 * Register a $route with $action for requests made with $method.
		 *
		 * @return void
		 **/
		public static function register($method, $route, $action, $userData=null)
		{
			$method = strtoupper($method);
			self::$ROUTES[$method][$route] = array("action"=>$action, "userData"=>$userData);
		}

		/**
		 * Prints out all the defined routes and their assigned actions for all
		 * HTTP methods. If $url is defined, also prints the result of matching
		 * that to URL each route.
		 *
		 * @param string $url
		 * @return void
		 **/
		public static function dump($url=null)
		{
			foreach ( self::$ROUTES as $method => $routes )
			{
				foreach ($routes as $route => $data)
				{
					echo '<pre>';
					if ( $url )
					{
						$path = request::path($url);
						$pattern = router::replace_placeholders($route);
						$params = null;
						$match = router::match($route, $path, $params) ? '**MATCH**' : 'no';
						echo "{$method} {$route} -> {$data["action"]}\n\t{$pattern}\n\t{$path}\n\tmatch:{$match}";
						if ( count ($params) )
						{
							echo "\n";
							var_dump($params);
						}
					}
					else
					{
						echo "{$method} {$route} -> {$data["action"]}";
					}
					echo '</pre><br>';
				}
			}
		}

		/**
		 * Scan all controllers in the project (including application, system and
		 * module controllers) and automatically generate and register routes based
		 * on their method signatures.
		 *
		 * @return void
		 **/
		public static function controllers()
		{
			$paths = Controller::paths();
			foreach ( $paths as $reference => $className )
			{
				$path = cascade::controller($reference);

				if ( $reference && !$path )
				{
					debug::error("Invalid path for the <code>{$reference}</code> controller");
				}

				require_once($path);

				try {
					$reflector = new ReflectionClass($className);
				} catch (ReflectionException $e) {
					debug::error("router::controllers(): class <code>{$className}</code> not found", array(
						'reference' => $reference,
						'path' => $path
					));
				}

				$methods = $reflector->getMethods();

				// The route to an index action with parameters will conflict with the
				// other actions in the controller, eg:
				//   /controller/:id
				//   /controller/action
				// This issue is so common that it would cripple the ability to register
				// controller routes automatically. To avoid that, we shift the index
				// action to the end of the methods array:

				foreach ( $methods as $key => $method )
					if ( preg_match('/^(get|post|put|patch|action)_index$/', $method->name ) )
					{
						$splice = array_splice($methods, $key, 1);
						$methods[] = $splice[0];
						break;
					}

				foreach ( $methods as $method )
				{
					if ( $method->isPublic() && !$method->isConstructor() && !$method->isStatic() )
					{
						preg_match('/^(get|post|put|patch)_([a-zA-Z0-9-_]+)$/', $method->name, $matches);
						if ( count($matches) == 3 && in_array(strtoupper($matches[1]), array_keys(self::$ROUTES)) )
						{
							$parameters = $method->getParameters();
							$action = strtolower("{$reference}:{$matches[2]}");
							$route = Controller::actionRoute($reference, $matches[2]);
							foreach ( $parameters as $parameter )
							{
								$route .= "/:{$parameter->name}";
								if ( $parameter->isOptional() ) $route .= '?';
							}
							$route = '/' . ltrim($route, '/');

							self::register($matches[1], $route, $action);
						}
					}
				}
			}
		}

		/**
		 * Replace all parameter placeholders in the route with the relevant regular
		 * expression.
		 *
		 * @param string $route
		 * @return string
		 **/
		private static function replace_placeholders($route)
		{
			foreach ( self::$_PLACEHOLDERS as $placeholder => $replacement )
			{
				$route = preg_replace($placeholder, $replacement, $route);
			}
			return '#^' . $route . '$#u';
		}

		/**
		 * Returns an array with the placeholders found in $route
		 *
		 * @return array
		 **/
		public static function placeholders($route)
		{
			$placeholders = array();

			preg_match_all('/\/?:[a-zA-Z0-9_]+\??/', $route, $matches);

			foreach ( $matches[0] as $match )
				$placeholders[] = trim($match, '/:?');

			return $placeholders;
		}

		/**
		 * Iterates through each registered route for the relevant $method, testing
		 * against $path. Returns the action string assigned to the first
		 * successful route match, or null if no match was found.
		 *
		 * Optionally, assigns the matched parameters to &$matched_parameters and
		 * the matched route to &$matched_route.
		 *
		 * @param string $method
		 * @param string $path
		 * @param array &$matched_parameters
		 * @param string &$matched_route
		 * @return string
		 **/
		static function resolve($method, $path, &$matched_parameters=null, &$matched_route=null)
		{
			foreach ( self::$ROUTES[strtoupper($method)] as $route => $data )
			{
				$match = self::match($route, $path, $parameters);
				if ( $match )
				{
					$placeholders = self::placeholders($route);

					// Set the placeholder names as the keys for the parameter array
					if ( count($placeholders) )
						$matched_parameters = array_combine( $placeholders,
							array_pad($parameters, count($placeholders), null) );
					else
						$matched_parameters = $parameters;

					$matched_route = $route;
					return $data;
				}
			}

			return null;
		}

		/**
		 * Returns true if $path segments match the initial segments in $route.
		 *
		 * @param string $root
		 * @param string $route
		 * @return boolean
		 */
		static function beginsWith($path, $route)
		{
			$path_segments = explode("/", $path);
			$path_length = count($path_segments);
			$route_segments = explode("/", $route);
			$match = true;
			for ($i = 0; $i < $path_length; $i++) {
				$match = $match && $path_segments[$i] == $route_segments[$i];
			}
			return $match;
		}

		/**
		 * Matches the passed $route against the passed $path, returning true for
		 * a positive match or false otherwise. Optionally assigns the matched
		 * parameter array to &$matched_parameters
		 *
		 * @param string $route
		 * @param string $path
		 * @param array &$matched_parameters
		 * @return boolean
		 **/
		static function match($route, $path, &$matched_parameters=null)
		{
			$matched_parameters = array();

			if ( !str::endsWith($route, '/') ) $route .= '/';

			$pattern = self::replace_placeholders($route);
			$match = preg_match($pattern, $path, $parameters);

			if ( $match )
			{

				// Discard the first match which is the complete string.
				array_shift($parameters);

				// Remove trailing slashes resulting from the parameter preg match for
				// the segment
				foreach ( $parameters as $key => $parameter )
					$parameters[$key] = trim($parameter, '/');

				$matched_parameters = $parameters;
				return true;
			}

			return false;
		}

	}
?>
