<?php
	class Main_controller extends Ksetup_controller
	{
		public function get_index()
		{
			$this->startpage();
		}

		public function get_notfound()
		{
			echo '<h1>404</h1><p>Page not found</p>';
		}
	}
?>
