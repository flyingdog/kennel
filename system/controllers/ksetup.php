<?php
	class Ksetup_controller extends Controller
	{
		const AUTH_REALM = 'ksetup';
		private static $DB;
		var $msg;
		
		public function __construct()
		{
			$this->template = new View('ksetup/layout');
		}
		
		public function get_index()
		{
			$this->get_modules();
		}
		
		public function post_login()
		{
			if (input::post('username') && input::post('password') &&
			auth::login(input::post('username'), input::post('password'), self::AUTH_REALM))
			{
				$this->get_index();
			}
			else
			{
				return $this->access_denied();
			}
		}
		
		private function access_denied()
		{
			$this->template->content = new View('ksetup/access-denied');
			$this->template->render();
			exit();
		}
		
		public function get_firststeps()
		{
			$view = new View('ksetup/first-steps');
			$view->render();
			exit();
		}
		
		public function get_startpage()
		{
			$this->template->content = new View('ksetup/startpage');
			$this->template->render();
		}
		
		public function get_modules()
		{
			if (!auth::check(self::AUTH_REALM)) $this->access_denied();
			
			$this->template->action = 'modules';
			$this->template->content = new View('ksetup/modules');
			$this->template->modules = Kennel::$MODULES;
			$this->template->render();
		}
		
		public function get_createmodels()
		{
			if ($this->hasUsers() && !auth::check(self::AUTH_REALM) ) $this->access_denied();
			
			$created = 0;
			$models = $this->getModels();
			
			foreach ($models as $id=>$model)
			{
				if (!$model['status'])
				{
					$created++;
					$filename = substr($model['info']['basename'], 0, strpos($model['info']['basename'], '.xml'));
					ORM::create($filename);
				}
			}
			
			$this->msg = "<strong>{$created} models</strong> created successfuly.";
			$this->get_database();
		}

		public function get_adminUser()
		{
			$this->template->content = new View('ksetup/admin-user');
			$this->template->action = 'admin-user';
			$firstUser = Crit('user')->retrieveFirst();
			$this->template->firstUser = !!$firstUser ? true : false;
			$this->template->render();
		}
		
		public function get_createAdmin()
		{
			$firstUser = Crit('user')->retrieveFirst();

			if (!!$firstUser) {
				flash::set('error', 'Cannot create admin user: database already has registered users.');
				request::redirect('ksetup/admin-user');
				return;
			}

			$modelName = Kennel::getSetting('auth', 'model_name');

			$user = Model::instance($modelName);
			$className = get_class($user);
			if (method_exists($className, 'init')) {
				call_user_func(array($className, 'init'));
				flash::set('created', "Admin user created. Identifier: <code>admin@example.com</code>, Password: <code>admin</code>");
			} else {
				flash::set('error', "Model class must implement the `init()` static method");
			}

			request::redirect('ksetup/admin-user');
		}

		private function hasUsers() {
			$conn = new  MySQL();

			$userModel = Kennel::getSetting('auth', 'model_name');
			$userSchema = new Schema($userModel);
			
			if (Kennel::getSetting('database', 'prefix')) {
				$userTable = Kennel::getSetting('database', 'prefix') . '_' . $userSchema->table;
			} else {
				$userTable = $userSchema->table;
			}

			$rs = $conn->query("SHOW TABLES");
			
			if ( gettype($rs) != "resource" ) return false;
			
			$tables = $conn->fetchAllArray($rs);

			foreach ($tables as $table) {
				if ($table[0] === $userTable) {
					return Crit($userModel)->count() > 0;
				}
			}

			return false;
		}
		
		public function get_database()
		{
			if ($this->hasUsers() && !auth::check(self::AUTH_REALM) ) $this->access_denied();
			
			$models = self::getModels();
			
			$this->template->action = 'database';
			$this->template->content = new View('ksetup/database');
			$this->template->models = $models;
			if($this->msg) $this->template->msg = $this->msg;
			$this->template->render();
		}
		
		public function get_settings()
		{
			if (!auth::check(self::AUTH_REALM)) $this->access_denied();
			
			$this->template->action = 'settings';
			$this->template->content = new View('ksetup/settings');
			$this->template->settings = Kennel::$SETTINGS;
			$this->template->render();
		}
		
		private function checkModel($model)
		{
		/*
			$schema = ORM::schema($model);
			$sql = "DESC `$schema->table`";
			
			$rs = self::$DB->query($sql);
			
			$schema = ORM::schema($model);
			print $schema->getCreateString();
			
			// compare existing database Table to Schema
			while ($row = self::$DB->fetch($rs))
			{
				print '<br />';
				print ' <strong>field:</strong>';
				print $row->Field;
				print ' <strong>type:</strong>';
				print $row->Type;
				print ' <strong>null:</strong>';
				print $row->Null;
				print '<br />';
			}
			
			print '<hr />';
		*/
		}
		
		private function getModels()
		{
			$models = array();
			
			// User Models
			$dir = Kennel::$ROOT_PATH . '/application/models/';
			if ( is_dir($dir) )
				foreach (scandir($dir) as $filename)
				{
					$path_info = pathinfo($filename);
					if ($path_info['extension'] && $path_info['extension'] == 'xml')
						$models[] = array('dir'=>$dir, 'info'=> $path_info, 'source'=>'application');
				}
			
			// Module Models
			foreach (Kennel::$MODULES as $module)
			{
				$dir = Kennel::$ROOT_PATH . "/modules/{$module}/models/";
				if (is_dir($dir))
					foreach (scandir($dir) as $filename)
					{
						$path_info = pathinfo($filename);
						if (isset($path_info['extension']) && $path_info['extension'] == 'xml')
							$models[] = array('dir'=>$dir, 'info'=> $path_info, 'source'=> $module);
					}
			}
			
			// System Models
			$dir = Kennel::$ROOT_PATH . "/system/models/";
			if (is_dir($dir))
				foreach (scandir($dir) as $filename)
				{
					$path_info = pathinfo($filename);
					if ($path_info['extension'] && $path_info['extension'] == 'xml')
						$models[] = array('dir'=>$dir, 'info'=> $path_info, 'source'=>'system');
				}
			
			self::$DB = new MySQL();
			$rs = self::$DB->query("SHOW TABLES");
			$tables = array();
			while ($row = self::$DB->fetchArray($rs))
			{
				$tables[] = $row[0];
			}
			
			foreach ($models as $id=>$model)
			{
				//self::checkModel($model['info']['filename']);
				// FILENAME was only introduced in PHP 5.2 and Terra Empresas is gay
				$filename = substr($model['info']['basename'], 0, strpos($model['info']['basename'], '.xml'));
				$schema = ORM::schema($filename);
				$result = array_search($schema->table, $tables);
				if ($result !== FALSE) $models[$id]['status'] = 'ok';
				else $models[$id]['status'] = '';
			}
			
			return $models;
		}

		public function get_migrations()
		{
			if (!auth::check(self::AUTH_REALM)) $this->access_denied();
			
			$this->template->action = 'migrations';
			$this->template->content = new View('ksetup/migrations');
			$this->template->render();
		}
		
		// public function get_backup()
		// {
		// 	if (!auth::check(self::AUTH_REALM)) $this->access_denied();
			
		// 	$dump = "";
			
		// 	$models = $this->getModels();
		// 	foreach ($this->getModels() as $model)
		// 	{
		// 		$model_name = substr($model['info']['basename'], 0, strpos($model['info']['basename'], '.xml'));
		// 		$schema = ORM::schema($model_name);
				
		// 		// Drop the table
		// 		$dump .= "DROP TABLE IF EXISTS `{$schema->table}`;\n";
				
		// 		// Create the table
		// 		$create_sql = $schema->getCreateString();
		// 		$dump .= "{$create_sql}\n\n";
				
		// 		// Dump the table data
		// 		$model_instances = ORM::retrieveAll($model_name);
		// 		foreach ($model_instances as $instance)
		// 		{
		// 			$insert_query = $instance->getSaveQuery(true);
		// 			$dump .= "{$insert_query}\n";
		// 		}
		// 		$dump .= "\n";
		// 	}
			
		// 	$this->template->content = new View('ksetup/backup');
		// 	$this->template->action = 'backup';
		// 	$this->template->dump = $dump;
		// 	$this->template->render();
		// }
	}
?>
