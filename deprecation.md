# API Deprecation

## v0.4.*b
	- _Module_ XML-based settings deprecated in favor of _SiteSettings_ DB-based settings

## v0.5.*
	- Route actions with dashes now resolve to camelCased method calls